package com.food.quibil.ui.features.sessionhistory;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;

import com.food.quibil.R;
import com.food.quibil.interfaces.FetchPastSessionsInterface;
import com.food.quibil.models.Session;
import com.food.quibil.network_layer.UserService;
import com.food.quibil.ui.features.sessionhistory.lists.SessionHistoryItemsRecyclerAdapter;

import java.util.ArrayList;

public class SessionHistoryActivity extends AppCompatActivity{

    ImageView ivBack;
    RecyclerView rvPastSessions;
    SessionHistoryItemsRecyclerAdapter sessionHistoryItemsRecyclerAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_session_history);

        ivBack = findViewById(R.id.ivBack);
        rvPastSessions = findViewById(R.id.rvPastSessions);

        sessionHistoryItemsRecyclerAdapter = new SessionHistoryItemsRecyclerAdapter(getApplicationContext(), new ArrayList<Session>());

        rvPastSessions.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        rvPastSessions.setAdapter(sessionHistoryItemsRecyclerAdapter);

        ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        UserService.getInstance(getApplicationContext()).getOlderSessions(new FetchPastSessionsInterface() {
            @Override
            public void onDataFetched(Session session) {
                sessionHistoryItemsRecyclerAdapter.addRow(session);
            }

            @Override
            public void onClearList() {
                sessionHistoryItemsRecyclerAdapter.clearRows();
            }

            @Override
            public void onError() {

            }
        });

    }
}
