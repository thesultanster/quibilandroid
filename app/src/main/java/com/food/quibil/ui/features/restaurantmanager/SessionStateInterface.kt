package com.food.quibil.ui.features.restaurantmanager

import com.food.quibil.models.Session

interface SessionStateInterface {
    fun checkedOut(session: Session)
}