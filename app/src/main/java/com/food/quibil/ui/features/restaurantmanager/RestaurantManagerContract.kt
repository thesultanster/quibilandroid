package com.food.quibil.ui.features.restaurantmanager

import com.food.quibil.models.Session

interface RestaurantManagerContract {

    interface View {
        fun sessionState_CheckedOut(session: Session)
    }

    interface Presenter {
        fun onStart(sessionId: String)
        fun onStop()
    }
}
