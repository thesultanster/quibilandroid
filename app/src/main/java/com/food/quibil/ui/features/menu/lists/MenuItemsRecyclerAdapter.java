package com.food.quibil.ui.features.menu.lists;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.food.quibil.R;
import com.food.quibil.interfaces.OnMenuItemSelectedInterface;
import com.food.quibil.models.MenuItem;
import com.squareup.picasso.Picasso;

import java.util.Collections;
import java.util.List;

public class MenuItemsRecyclerAdapter extends RecyclerView.Adapter<MenuItemsRecyclerAdapter.MyViewHolder> {

    // emptyList takes care of null pointer exception
    List<MenuItem> menuItems = Collections.emptyList();
    LayoutInflater inflator;
    Context context;

    OnMenuItemSelectedInterface onMenuItemSelectedInterface;


    public MenuItemsRecyclerAdapter(Context context, List<MenuItem> menuItems, OnMenuItemSelectedInterface onMenuItemSelectedInterface) {
        this.context = context;
        this.onMenuItemSelectedInterface = onMenuItemSelectedInterface;
        inflator = LayoutInflater.from(context);
        this.menuItems = menuItems;
    }

    public void addRow(MenuItem menuItem) {
        menuItems.add(menuItem);
        notifyItemInserted(getItemCount() - 1);
    }

    public void clearRows() {
        menuItems.clear();
        notifyDataSetChanged();
    }

    // Called when the recycler view needs to create a new row
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        final View view = inflator.inflate(R.layout.row_menu_category, parent, false);
        final MyViewHolder holder = new MyViewHolder(view, context, new MyViewHolder.MyViewHolderClicks() {

            public void rowClick(View caller, int position) {
                Log.d("rowClick", "rowClicks");

                // Pass selected category down the interface
                onMenuItemSelectedInterface.onItemSelected(position, menuItems.get(position));

                notifyDataSetChanged(); // notify adapter

            }


        });
        return holder;
    }

    // Setting up the data for each row
    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        MenuItem current = menuItems.get(position);

        holder.tvTitle.setText(current.name);

        if(current.itemPhotoUrl != null && !current.itemPhotoUrl.equals("")) {
            Picasso.with(context).load(current.itemPhotoUrl).into(holder.ivItemPicture);
        }
    }


    @Override
    public int getItemCount() {
        return menuItems.size();
    }

    // Created my custom view holder
    public static class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView tvTitle;
        ImageView ivItemPicture;
        LinearLayout rlRow;
        Context context;

        public MyViewHolderClicks mListener;

        // itemView will be my own custom layout View of the row
        public MyViewHolder(View itemView, Context context, MyViewHolderClicks listener) {
            super(itemView);
            this.mListener = listener;
            this.context = context;

            ivItemPicture = itemView.findViewById(R.id.ivItemPicture);
            tvTitle = itemView.findViewById(R.id.tvTitle);
            rlRow = itemView.findViewById(R.id.rlRow);

            itemView.setOnClickListener(this);

        }

        @Override
        public void onClick(View v) {
            switch (v.getId()) {

                default:
                    mListener.rowClick(v, getAdapterPosition());
                    break;
            }
        }

        public interface MyViewHolderClicks {
            void rowClick(View caller, int position);
        }
    }


}