package com.food.quibil.ui.features.restaurantmanager

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.util.Log
import android.view.View
import android.widget.Toast
import com.food.quibil.R
import com.food.quibil.ui.features.finishandpay.FinishAndPayActivity
import com.food.quibil.ui.features.menu.OrderActivity
import com.food.quibil.activities.ScanActivity
import com.food.quibil.data_layer.UserOrder
import com.food.quibil.interfaces.OnCheckOutUserFromRestaurantInterface
import com.food.quibil.models.Order
import com.food.quibil.models.Session
import com.food.quibil.network_layer.OrderService
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import kotlinx.android.synthetic.main.activity_restaurant_manager.*

class RestaurantManagerActivity : AppCompatActivity(), RestaurantManagerContract.View {
    var TAG = "RestaurantManagerActivity"

    lateinit var postListener: ValueEventListener
    lateinit var session: Session
    lateinit var restaurantManagerPresenter: RestaurantManagerContract.Presenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_restaurant_manager)
        restaurantManagerPresenter = RestaurantManagerPresenter(this)

        btnFinishAndPay.setOnClickListener {
            if( session.orders != null && session.orders.size != 0 ) {
                val intent = Intent(applicationContext, FinishAndPayActivity::class.java)
                startActivity(intent)
            } else {
                Toast.makeText(applicationContext, "You haven't ordered anything!", Toast.LENGTH_LONG).show()
            }
        }

        btnMenu.setOnClickListener {
            val intent = Intent(applicationContext, OrderActivity::class.java)
            startActivity(intent)
        }

        btnExit.setOnClickListener {
            restaurantManagerPresenter.checkoutUser(applicationContext)
        }

    }


    override fun onStart() {
        super.onStart()

        postListener = object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                session = dataSnapshot.getValue(Session::class.java)!!
                Log.d(TAG, "postListener:onDataChange: " + session.state)

                // If session is checked out, then check out user
                if(session.state == "CHECKED_OUT"){
                    restaurantManagerPresenter.checkoutUser(applicationContext)
                    return
                }

                // Cache
                UserOrder.getInstance(applicationContext).persistSessionId(session.id)
                UserOrder.getInstance(applicationContext).persistSessionState(session.state)
                UserOrder.getInstance(applicationContext).cacheRestaurantId(session.restaurantId)

                // Update order list in UserOrder Service
                if(session.orders == null){
                    UserOrder.getInstance(applicationContext).order = Order()
                    btnFinishAndPay.visibility = View.GONE
                } else {
                    var orders = Order()
                    orders.orderedItems = session.orders
                    UserOrder.getInstance(applicationContext).order = orders

                    // Only show the exit button if they havn't ordered anything
                    if(session.orders.size > 0){
                        btnExit.visibility = View.GONE
                        btnFinishAndPay.visibility = View.VISIBLE
                    } else {
                        btnExit.visibility = View.VISIBLE
                        btnFinishAndPay.visibility = View.GONE
                    }
                }
            }

            override fun onCancelled(databaseError: DatabaseError) {
                // Getting Post failed, log a message
                Log.e(TAG, "postListener:onCancelled", databaseError.toException())
            }
        }

        val currentSessionId = UserOrder.getInstance(applicationContext).sessionId
        if (currentSessionId != null && currentSessionId != "") {
            FirebaseDatabase.getInstance().reference
                    .child("order_sessions")
                    .child(UserOrder.getInstance(applicationContext).sessionId)
                    .addValueEventListener(postListener)
        } else {


            // Session is expired, show dialog
            AlertDialog.Builder(this)
                    .setMessage("Session Expired")
                    .setPositiveButton("Exit") { dialogInterface, i ->
                        // Go back to scan activity
                        val cameraIntent = Intent(applicationContext, ScanActivity::class.java)
                        startActivity(cameraIntent)
                        finish()
                    }
                    .setCancelable(false)
                    .create()
                    .show()
        }

    }

    override fun onStop() {
        super.onStop()

        FirebaseDatabase.getInstance().reference.removeEventListener(postListener)
    }

    override fun goToScanActivity() {
        // Go to scan activity
        val intent = Intent(applicationContext, ScanActivity::class.java)
        startActivity(intent)
        finish()
    }

}
