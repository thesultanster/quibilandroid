package com.food.quibil.ui.features.finishandpay;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.braintreepayments.api.dropin.DropInActivity;
import com.braintreepayments.api.dropin.DropInRequest;
import com.braintreepayments.api.dropin.DropInResult;
import com.food.quibil.R;
import com.food.quibil.activities.ScanActivity;
import com.food.quibil.adapters.ReceiptItemsRecyclerAdapter;
import com.food.quibil.data_layer.UserOrder;
import com.food.quibil.interfaces.CloseOutClientInterface;
import com.food.quibil.interfaces.GenerateBraintreeClientTokenInterface;
import com.food.quibil.network_layer.AuthService;
import com.food.quibil.network_layer.OrderService;
import com.food.quibil.utils.AppState;

import java.text.NumberFormat;
import java.util.Locale;

import de.hdodenhof.circleimageview.CircleImageView;

public class FinishAndPayActivity extends AppCompatActivity {
    String TAG = "FinishAndPayActivity";
    int BT_CODE = 12345;

    Button btnFinishAndPay;
    TextView tvTotalCartPrice;
    TextView tvSubtotal;
    ImageView ivBack;
    TextView tvTip;
    TextView tvNoTip;
    TextView tv10Tip;
    TextView tv15Tip;
    TextView tv20Tip;
    RelativeLayout rlNoTip;
    RelativeLayout rl10Tip;
    RelativeLayout rl15Tip;
    RelativeLayout rl20Tip;
    CircleImageView civNoTip;
    CircleImageView civ10Tip;
    CircleImageView civ15Tip;
    CircleImageView civ20Tip;
    RecyclerView rvMenuItems;
    ReceiptItemsRecyclerAdapter receipttemsRecyclerAdapter;
    int currentTipIndex = 0;
    ProgressDialog progressDialog;

    CircleImageView[] civTips;
    TextView[] tvTips;
    RelativeLayout[] rlTips;
    int[] tip;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_finish_and_pay);

        rvMenuItems = findViewById(R.id.rvMenuItems);
        tvTotalCartPrice = findViewById(R.id.tvTotalCartPrice);
        tvSubtotal = findViewById(R.id.tvSubtotal);
        ivBack = findViewById(R.id.ivBack);
        tvTip = findViewById(R.id.tvTip);
        tvNoTip = findViewById(R.id.tvNoTip);
        tv10Tip = findViewById(R.id.tv10Tip);
        tv15Tip = findViewById(R.id.tv15Tip);
        tv20Tip = findViewById(R.id.tv20Tip);
        rlNoTip = findViewById(R.id.rlNoTip);
        rl10Tip = findViewById(R.id.rl10Tip);
        rl15Tip = findViewById(R.id.rl15Tip);
        rl20Tip = findViewById(R.id.rl20Tip);
        civNoTip = findViewById(R.id.civNoTip);
        civ10Tip = findViewById(R.id.civ10Tip);
        civ15Tip = findViewById(R.id.civ15Tip);
        civ20Tip = findViewById(R.id.civ20Tip);
        btnFinishAndPay = findViewById(R.id.btnFinishAndPay);

        // Setup recyclerview and adapter
        receipttemsRecyclerAdapter = new ReceiptItemsRecyclerAdapter(getApplicationContext());
        rvMenuItems.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        rvMenuItems.setAdapter(receipttemsRecyclerAdapter);
        rvMenuItems.setNestedScrollingEnabled(false);
        receipttemsRecyclerAdapter.setOrderedItems(UserOrder.getInstance(getApplicationContext()).getOrder().orderedItems);


        btnFinishAndPay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                progressDialog = new ProgressDialog(FinishAndPayActivity.this);
                progressDialog.setMessage("Processing Payment");
                progressDialog.show();
                progressDialog.setCancelable(false);


                AuthService.getInstance(getApplicationContext()).generateBraintreeClientToken(new GenerateBraintreeClientTokenInterface() {
                    @Override
                    public void onClientTokenCreated(String clientToken) {
                        AppState.getInstance(getApplicationContext()).cacheBraintreeClientToken(clientToken);

                        DropInRequest dropInRequest = new DropInRequest()
                                .clientToken(clientToken);
                        startActivityForResult(dropInRequest.getIntent(getApplicationContext()), BT_CODE);


                    }

                    @Override
                    public void onError() {
                        progressDialog.dismiss();
                    }
                });
            }
        });

        // Format and set total cart price
        NumberFormat n = NumberFormat.getCurrencyInstance(Locale.US);
        String s = n.format(UserOrder.getInstance(getApplicationContext()).getTotalCartPrice() / 100.0);
        tvTotalCartPrice.setText(s);
        tvSubtotal.setText(s);

        ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });


        // Set Tip Views
        civTips = new CircleImageView[]{civNoTip, civ10Tip, civ15Tip, civ20Tip};
        tvTips = new TextView[]{tvNoTip, tv10Tip, tv15Tip, tv20Tip};
        rlTips = new RelativeLayout[]{rlNoTip, rl10Tip, rl15Tip, rl20Tip};
        tip = new int[]{0, 15, 18, 22};
        for (int i = 0; i < rlTips.length; i++) {

            final int tempIndex = i;
            final double finalServiceCost = UserOrder.getInstance(getApplicationContext()).getTotalCartPrice();
            rlTips[i].setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    // If user clicked on already selected tip option
                    // Then unselect current tip option and automatically select No tip
                    if (tempIndex == currentTipIndex) {
                        // Unselect current tip option
                        civTips[tempIndex].setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.circle_accent_border));
                        tvTips[tempIndex].setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.colorAccent));

                        // Select No Tip
                        Drawable color = new ColorDrawable(getResources().getColor(R.color.colorAccent));
                        civTips[0].setImageDrawable(color);
                        tvTips[0].setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.white));

                        NumberFormat n = NumberFormat.getCurrencyInstance(Locale.US);
                        String s = n.format(finalServiceCost / 100.0);
                        tvTotalCartPrice.setText(s);
                        tvTip.setText("$0.00");

                        currentTipIndex = 0;
                    }
                    // Otherwise user clicks on a new tip option
                    // Then select new option and unselect old option
                    else {
                        // Unselect current tip option
                        civTips[currentTipIndex].setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.circle_accent_border));
                        tvTips[currentTipIndex].setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.colorAccent));

                        // Select New Option
                        Drawable color = new ColorDrawable(getResources().getColor(R.color.colorAccent));
                        civTips[tempIndex].setImageDrawable(color);
                        tvTips[tempIndex].setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.white));

                        currentTipIndex = tempIndex;

                        NumberFormat n = NumberFormat.getCurrencyInstance(Locale.US);
                        String s = n.format((finalServiceCost * (1.00 + (tip[currentTipIndex] / 100.0))) / 100.0);
                        tvTotalCartPrice.setText(s);

                        String tipText = n.format((finalServiceCost * ((tip[currentTipIndex] / 100.0))) / 100.0);
                        tvTip.setText(tipText);

                    }


                }
            });
        }

        currentTipIndex = 0;
        rlTips[currentTipIndex].performClick();


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == BT_CODE) {

            if (resultCode == Activity.RESULT_OK) {
                DropInResult result = data.getParcelableExtra(DropInResult.EXTRA_DROP_IN_RESULT);
                result.getPaymentMethodNonce().getNonce();

                progressDialog.setMessage("Sending Payment");

                OrderService.getInstance(getApplicationContext()).closeOutClient(result.getPaymentMethodNonce().getNonce(), UserOrder.getInstance(getApplicationContext()).getSessionId(), (long) (UserOrder.getInstance(getApplicationContext()).getTotalCartPrice() * (1.00 + (tip[currentTipIndex] / 100.0))), new CloseOutClientInterface() {
                    @Override
                    public void onClientClosedOut() {
                        Toast.makeText(FinishAndPayActivity.this, "Success", Toast.LENGTH_SHORT).show();

                        progressDialog.dismiss();

                        UserOrder.getInstance(getApplicationContext()).persistSessionId("");
                        UserOrder.getInstance(getApplicationContext()).persistSessionState("CHECKED_OUT");
                        UserOrder.getInstance(getApplicationContext()).cacheRestaurantId("");

                        // Go to scan activity
                        Intent intent = new Intent(getApplicationContext(), ScanActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                        finish();

                    }

                    @Override
                    public void onError() {
                        progressDialog.dismiss();
                        Toast.makeText(FinishAndPayActivity.this, "Error Sending Payment", Toast.LENGTH_SHORT).show();
                    }
                });


                // use the result to update your UI and send the payment method nonce to your server
            } else if (resultCode == Activity.RESULT_CANCELED) {
                progressDialog.dismiss();

                // the user canceled
            } else {
                // handle errors here, an exception may be available in
                Exception error = (Exception) data.getSerializableExtra(DropInActivity.EXTRA_ERROR);

                progressDialog.dismiss();
                Toast.makeText(FinishAndPayActivity.this, "Error Processing", Toast.LENGTH_SHORT).show();

            }
        }
    }
}
