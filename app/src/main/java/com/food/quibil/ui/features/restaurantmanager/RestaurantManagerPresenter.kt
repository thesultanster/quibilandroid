package com.food.quibil.ui.features.restaurantmanager

import com.food.quibil.models.Session
import com.food.quibil.network_layer.SessionManager

class RestaurantManagerPresenter(var restaurantManagerView: RestaurantManagerContract.View) : RestaurantManagerContract.Presenter,
SessionStateInterface{

    private lateinit var sessionManager: SessionManager
    private lateinit var sessionId: String

    override fun onStart(sessionId: String) {
        this.sessionId = sessionId
        sessionManager = SessionManager()
        sessionManager.init(this, sessionId)
    }

    override fun onStop() {
        sessionManager.release()
    }


    override fun checkedOut(session: Session) {
        restaurantManagerView.sessionState_CheckedOut(session)
    }


}