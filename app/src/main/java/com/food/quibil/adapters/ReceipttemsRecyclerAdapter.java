package com.food.quibil.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.food.quibil.R;
import com.food.quibil.data_layer.UserOrder;
import com.food.quibil.interfaces.RoundItemInterface;
import com.food.quibil.interfaces.RoundItemRowType;
import com.food.quibil.models.AddOnItem;
import com.food.quibil.models.MenuItem;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class ReceipttemsRecyclerAdapter extends RecyclerView.Adapter {

    // emptyList takes care of null pointer exception
    LayoutInflater inflator;
    Context context;

    private List<RoundItemRowType> dataSet;


    public ReceipttemsRecyclerAdapter(Context context) {
        this.context = context;
        this.dataSet = new ArrayList<>();
        this.inflator = LayoutInflater.from(context);
    }

    public void setMenuItems(List<MenuItem> menuItems) {

        for (MenuItem menuItem : menuItems) {
            dataSet.add(menuItem);

            for (AddOnItem addOnItem : menuItem.addOnItems) {
                dataSet.add(addOnItem);
            }

        }


        notifyDataSetChanged();
    }

    public void addRow(MenuItem menuItem) {
        //menuItems.add(menuItem);
        notifyItemInserted(getItemCount() - 1);
    }

    public void clearRows() {
        //menuItems.clear();
        notifyDataSetChanged();
    }


    // Called when the recycler view needs to create a new row
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        if (viewType == RoundItemRowType.ITEM) {
            View view = inflator.inflate(R.layout.row_receipt_item, parent, false);
            final ItemViewHolder holder = new ItemViewHolder(view, new ItemViewHolder.MyViewHolderClicks() {

                public void rowClick(View caller, int position) {
                    Log.d("rowClick", "rowClicks");

                }



            });
            return holder;
        }
        // i.e. if(viewType == RoundItemRowType.SUB_ITEM)
        else {
            View view = inflator.inflate(R.layout.row_receipt_sub_item, parent, false);
            final SubItemViewHolder holder = new SubItemViewHolder(view, new SubItemViewHolder.MyViewHolderClicks() {

                public void rowClick(View caller, int position) {
                    Log.d("rowClick", "rowClicks");

                }


            });
            return holder;
        }


    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof ItemViewHolder) {
            MenuItem current = (MenuItem) dataSet.get(position);
            ((ItemViewHolder) holder).tvName.setText(current.name);

            // Format and set price
            NumberFormat n = NumberFormat.getCurrencyInstance(Locale.US);
            String s = n.format(current.price  / 100.0);
            ((ItemViewHolder) holder).tvItemPrice.setText(s);

        } else if (holder instanceof SubItemViewHolder) {
            AddOnItem current = (AddOnItem) dataSet.get(position);
            ((SubItemViewHolder) holder).tvName.setText(current.name);

            // Format and set price
            NumberFormat n = NumberFormat.getCurrencyInstance(Locale.US);
            String s = n.format(current.price/ 100.0);
            ((SubItemViewHolder) holder).tvItemPrice.setText(s);
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (dataSet.get(position) instanceof MenuItem) {
            return RoundItemRowType.ITEM;
        } else if (dataSet.get(position) instanceof AddOnItem) {
            return RoundItemRowType.SUB_ITEM;
        } else {
            return -1;
        }
    }


    @Override
    public int getItemCount() {
        return dataSet.size();
    }

    // Created my custom view holder
    public static class ItemViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView tvName;
        TextView tvItemPrice;
        RelativeLayout rlRow;

        public MyViewHolderClicks mListener;

        // itemView will be my own custom layout View of the row
        public ItemViewHolder(View itemView, MyViewHolderClicks listener) {
            super(itemView);
            this.mListener = listener;

            tvItemPrice = itemView.findViewById(R.id.tvItemPrice);
            tvName = (TextView) itemView.findViewById(R.id.tvName);
            rlRow = (RelativeLayout) itemView.findViewById(R.id.rlRow);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                default:
                    mListener.rowClick(v, getAdapterPosition());
                    break;
            }
        }

        public interface MyViewHolderClicks {
            void rowClick(View caller, int position);
        }
    }

    // Created my custom view holder
    public static class SubItemViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView tvName;
        TextView tvItemPrice;
        RelativeLayout rlRow;

        public MyViewHolderClicks mListener;

        // itemView will be my own custom layout View of the row
        public SubItemViewHolder(View itemView, MyViewHolderClicks listener) {
            super(itemView);
            this.mListener = listener;

            tvItemPrice = itemView.findViewById(R.id.tvItemPrice);
            tvName = (TextView) itemView.findViewById(R.id.tvName);
            rlRow = (RelativeLayout) itemView.findViewById(R.id.rlRow);

            itemView.setOnClickListener(this);

        }

        @Override
        public void onClick(View v) {
            switch (v.getId()) {

                default:
                    mListener.rowClick(v, getAdapterPosition());
                    break;
            }
        }

        public interface MyViewHolderClicks {
            void rowClick(View caller, int position);
        }
    }


}