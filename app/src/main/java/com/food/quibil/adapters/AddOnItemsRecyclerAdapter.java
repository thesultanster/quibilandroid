package com.food.quibil.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.food.quibil.R;
import com.food.quibil.interfaces.OnAddOnItemSelectedInterface;
import com.food.quibil.models.AddOnItem;
import com.food.quibil.models.MenuItem;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Locale;

public class AddOnItemsRecyclerAdapter extends RecyclerView.Adapter<AddOnItemsRecyclerAdapter.MyViewHolder> {

    // emptyList takes care of null pointer exception
    List<AddOnItem> addOnItems = Collections.emptyList();
    List<Integer> itemCount = Collections.emptyList();
    LayoutInflater inflator;
    Context context;

    OnAddOnItemSelectedInterface addOnItemSelectedInterface;


    public AddOnItemsRecyclerAdapter(Context context, List<AddOnItem> addOnItems, OnAddOnItemSelectedInterface addOnItemSelectedInterface) {
        this.context = context;
        this.addOnItemSelectedInterface = addOnItemSelectedInterface;
        this.addOnItems = addOnItems;
        this.itemCount = new ArrayList<>();
        inflator = LayoutInflater.from(context);
    }

    public void addRow(AddOnItem addOnItem) {
        addOnItems.add(addOnItem);
        itemCount.add(0);
        notifyItemInserted(getItemCount() - 1);
    }

    public void clearRows() {
        addOnItems.clear();
        notifyDataSetChanged();
    }

    // Called when the recycler view needs to create a new row
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        final View view = inflator.inflate(R.layout.row_addon_item, parent, false);
        final MyViewHolder holder = new MyViewHolder(view, context, new MyViewHolder.MyViewHolderClicks() {

            public void rowClick(View caller, int position) {
                Log.d("rowClick", "rowClicks");

                // Pass selected category down the interface
                // onMenuItemSelectedInterface.onItemSelected(position, addOnItems.get(position));


            }

            @Override
            public void increment(View caller, int position) {

                if(itemCount.get(position) >= 0) {
                    addOnItemSelectedInterface.onAddOnItemIncremented(position, addOnItems.get(position));
                    itemCount.set(position, itemCount.get(position) + 1);
                    notifyDataSetChanged();
                }
            }

            @Override
            public void decrement(View caller, int position) {
                if(itemCount.get(position) != 0) {
                    addOnItemSelectedInterface.onAddOnItemDecremented(position, addOnItems.get(position));
                    itemCount.set(position, itemCount.get(position) - 1);
                    notifyDataSetChanged();
                }
            }


        });
        return holder;
    }

    // Setting up the data for each row
    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        AddOnItem current = addOnItems.get(position);

        NumberFormat n = NumberFormat.getCurrencyInstance(Locale.US);
        String s = n.format(current.price / 100.0);

        holder.tvPrice.setText("+"+s);
        holder.tvCategoryTitle.setText(current.name);
        holder.tvCount.setText(""+itemCount.get(position));
    }


    @Override
    public int getItemCount() {
        return addOnItems.size();
    }

    // Created my custom view holder
    public static class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        ImageView ivAdd;
        ImageView ivSubtract;
        TextView tvCategoryTitle;
        TextView tvCount;
        TextView tvPrice;
        RelativeLayout rlRow;
        Context context;

        public MyViewHolderClicks mListener;

        // itemView will be my own custom layout View of the row
        public MyViewHolder(View itemView, Context context, MyViewHolderClicks listener) {
            super(itemView);
            this.mListener = listener;
            this.context = context;

            tvCategoryTitle =  itemView.findViewById(R.id.tvCategoryTitle);
            ivAdd =  itemView.findViewById(R.id.ivAdd);
            ivSubtract =  itemView.findViewById(R.id.ivSubtract);
            rlRow = itemView.findViewById(R.id.rlRow);
            tvCount = itemView.findViewById(R.id.tvCount);
            tvPrice = itemView.findViewById(R.id.itemPrice);


            itemView.setOnClickListener(this);
            ivAdd.setOnClickListener(this);
            ivSubtract.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.ivSubtract:
                    mListener.decrement(v, getAdapterPosition());
                    break;
                case R.id.ivAdd:
                    mListener.increment(v, getAdapterPosition());
                    break;
                default:
                    mListener.rowClick(v, getAdapterPosition());
                    break;
            }
        }

        public interface MyViewHolderClicks {
            void rowClick(View caller, int position);
            void increment(View caller, int position);
            void decrement(View caller, int position);
        }
    }


}