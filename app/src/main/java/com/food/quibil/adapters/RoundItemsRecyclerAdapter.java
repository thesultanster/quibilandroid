package com.food.quibil.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.food.quibil.R;
import com.food.quibil.interfaces.RoundItemInterface;
import com.food.quibil.interfaces.RoundItemRowType;
import com.food.quibil.models.AddOnItem;
import com.food.quibil.models.MenuItem;
import com.food.quibil.models.OrderedItem;

import java.util.ArrayList;
import java.util.List;

public class RoundItemsRecyclerAdapter extends RecyclerView.Adapter {

    // emptyList takes care of null pointer exception
    LayoutInflater inflator;
    Context context;

    private List<RoundItemRowType> dataSet;


    RoundItemInterface roundItemInterface;


    public RoundItemsRecyclerAdapter(Context context, RoundItemInterface roundItemInterface) {
        this.context = context;
        this.roundItemInterface = roundItemInterface;
        this.dataSet = new ArrayList<>();
        this.inflator = LayoutInflater.from(context);
    }

    public void setOrderedItems(List<OrderedItem> orderedItems) {

        for (OrderedItem orderedItem : orderedItems) {
            dataSet.add(orderedItem);

            for (AddOnItem addOnItem : orderedItem.addOnItems) {
                dataSet.add(addOnItem);
            }

        }


        notifyDataSetChanged();
    }


    // Called when the recycler view needs to create a new row
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        if (viewType == RoundItemRowType.ITEM) {
            View view = inflator.inflate(R.layout.row_round_item, parent, false);
            final ItemViewHolder holder = new ItemViewHolder(view, new ItemViewHolder.MyViewHolderClicks() {

                public void rowClick(View caller, int position) {
                    Log.d("rowClick", "rowClicks");

                }

                @Override
                public void removeItem(View caller, int position) {

                    roundItemInterface.onUserWantsToRemoveItem(position, dataSet.get(position));

                    // Get current item
                    OrderedItem orderedItem = (OrderedItem) dataSet.get(position);
                    int numberOfAddOnItems = orderedItem.addOnItems.size();

                    // Remove items from adapter
                    // Remove the orderedItem
                    dataSet.remove(position);
                    // Call remove in the same position by iterating the numeber of add on items we have
                    // This will remove all add on items from list as well
                    for(int i = 0; i < numberOfAddOnItems; i++){
                        dataSet.remove(position);
                    }

                    notifyDataSetChanged();
                }


            });
            return holder;
        }
        // i.e. if(viewType == RoundItemRowType.SUB_ITEM)
        else {
            View view = inflator.inflate(R.layout.row_round_sub_item, parent, false);
            final SubItemViewHolder holder = new SubItemViewHolder(view, new SubItemViewHolder.MyViewHolderClicks() {

                public void rowClick(View caller, int position) {
                    Log.d("rowClick", "rowClicks");

                }

                @Override
                public void removeItem(View caller, int position) {
                    roundItemInterface.onUserWantsToRemoveItem(position, dataSet.get(position));

                    // Remove the addOnItem
                    dataSet.remove(position);
                    notifyDataSetChanged();
                }


            });
            return holder;
        }


    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof ItemViewHolder) {
            OrderedItem current = (OrderedItem) dataSet.get(position);
            ((ItemViewHolder) holder).tvName.setText(current.name);
        } else if (holder instanceof SubItemViewHolder) {
            AddOnItem current = (AddOnItem) dataSet.get(position);
            ((SubItemViewHolder) holder).tvName.setText(current.name);
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (dataSet.get(position) instanceof OrderedItem) {
            return RoundItemRowType.ITEM;
        } else if (dataSet.get(position) instanceof AddOnItem) {
            return RoundItemRowType.SUB_ITEM;
        } else {
            return -1;
        }
    }


    @Override
    public int getItemCount() {
        return dataSet.size();
    }

    // Created my custom view holder
    public static class ItemViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        ImageView ivRemoveItem;
        TextView tvName;
        RelativeLayout rlRow;

        public MyViewHolderClicks mListener;

        // itemView will be my own custom layout View of the row
        public ItemViewHolder(View itemView, MyViewHolderClicks listener) {
            super(itemView);
            this.mListener = listener;

            ivRemoveItem = itemView.findViewById(R.id.ivRemoveItem);
            tvName = (TextView) itemView.findViewById(R.id.tvName);
            rlRow = (RelativeLayout) itemView.findViewById(R.id.rlRow);

            itemView.setOnClickListener(this);
            ivRemoveItem.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.ivRemoveItem:
                    mListener.removeItem(v, getAdapterPosition());
                    break;
                default:
                    mListener.rowClick(v, getAdapterPosition());
                    break;
            }
        }

        public interface MyViewHolderClicks {
            void rowClick(View caller, int position);

            void removeItem(View caller, int position);
        }
    }

    // Created my custom view holder
    public static class SubItemViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        ImageView ivRemoveItem;
        TextView tvName;
        RelativeLayout rlRow;

        public MyViewHolderClicks mListener;

        // itemView will be my own custom layout View of the row
        public SubItemViewHolder(View itemView, MyViewHolderClicks listener) {
            super(itemView);
            this.mListener = listener;

            ivRemoveItem = itemView.findViewById(R.id.ivRemoveItem);
            tvName = (TextView) itemView.findViewById(R.id.tvName);
            rlRow = (RelativeLayout) itemView.findViewById(R.id.rlRow);

            itemView.setOnClickListener(this);
            ivRemoveItem.setOnClickListener(this);

        }

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.ivRemoveItem:
                    mListener.removeItem(v, getAdapterPosition());
                    break;
                default:
                    mListener.rowClick(v, getAdapterPosition());
                    break;
            }
        }

        public interface MyViewHolderClicks {
            void rowClick(View caller, int position);

            void removeItem(View caller, int position);
        }
    }


}