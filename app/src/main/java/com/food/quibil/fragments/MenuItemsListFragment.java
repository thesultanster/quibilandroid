package com.food.quibil.fragments;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


import com.food.quibil.R;
import com.food.quibil.activities.MenuItemActivity;
import com.food.quibil.adapters.MenuItemsRecyclerAdapter;
import com.food.quibil.interfaces.FetchRestaurantMenuItemsInterface;
import com.food.quibil.interfaces.OnMenuItemSelectedInterface;
import com.food.quibil.models.MenuCategory;
import com.food.quibil.models.MenuItem;
import com.food.quibil.models.Restaurant;
import com.food.quibil.network_layer.MenuService;

import java.util.ArrayList;


public class MenuItemsListFragment extends Fragment {

    RecyclerView rvMenuItems;
    MenuItemsRecyclerAdapter menuItemsRecyclerAdapter;

    Typeface openFace;


    Restaurant currentRestaurant;
    MenuItem currentSelectedItem;
    MenuCategory currentCategory;



    public MenuItemsListFragment() {
        // Required empty public constructor
    }

    public static MenuItemsListFragment newInstance(MenuCategory currentCategory) {
        MenuItemsListFragment frag = new MenuItemsListFragment();
        Bundle args = new Bundle();
        args.putSerializable("currentCategory", currentCategory);
        frag.setArguments(args);
        return frag;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        openFace = Typeface.createFromAsset(getActivity().getAssets(), "fonts/OpenSans-Light.ttf");
        currentCategory = (MenuCategory) getArguments().getSerializable("currentCategory");

        menuItemsRecyclerAdapter = new MenuItemsRecyclerAdapter(getContext(), new ArrayList<MenuItem>(), new OnMenuItemSelectedInterface(){

            @Override
            public void onItemSelected(int position, MenuItem menuItem) {
                currentSelectedItem = menuItem;

                Intent intent = new Intent(getContext(), MenuItemActivity.class);
                intent.putExtra("menuItem", menuItem);
                startActivity(intent);

            }
        });



    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_menu_items_list, container, false);

        rvMenuItems = (RecyclerView) view.findViewById(R.id.rvMenuItems);
        rvMenuItems.setLayoutManager(new LinearLayoutManager(getActivity()));
        rvMenuItems.setAdapter(menuItemsRecyclerAdapter);

        return view;
    }


    @Override
    public void onStart() {
        super.onStart();


        MenuService.getInstance(getContext()).getMenuItems(currentCategory.restaurantId, currentCategory.id, new FetchRestaurantMenuItemsInterface() {
            @Override
            public void onDataFetched(MenuItem menuItem) {
                menuItemsRecyclerAdapter.addRow(menuItem);
            }

            @Override
            public void onError() {

            }
        });
    }

    @Override
    public void onStop() {
        super.onStop();

        menuItemsRecyclerAdapter.clearRows();
    }
}
