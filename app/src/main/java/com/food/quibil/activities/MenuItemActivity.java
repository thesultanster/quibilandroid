package com.food.quibil.activities;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.food.quibil.R;
import com.food.quibil.adapters.AddOnItemsRecyclerAdapter;
import com.food.quibil.data_layer.CurrentUser;
import com.food.quibil.data_layer.UserOrder;
import com.food.quibil.interfaces.AddItemToOrderInterface;
import com.food.quibil.interfaces.FetchRestaurantItemAddOnsInterface;
import com.food.quibil.interfaces.OnAddOnItemSelectedInterface;
import com.food.quibil.interfaces.OnRequestFoodForSessionInterface;
import com.food.quibil.interfaces.UpdateUserToDatabaseInterface;
import com.food.quibil.models.AddOnItem;
import com.food.quibil.models.MenuItem;
import com.food.quibil.models.User;
import com.food.quibil.network_layer.MenuService;
import com.food.quibil.network_layer.OrderService;
import com.food.quibil.ui.features.restaurantmanager.RestaurantManagerActivity;
import com.rey.material.widget.Button;
import com.squareup.picasso.Picasso;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Locale;

public class MenuItemActivity extends AppCompatActivity implements AddItemToOrderInterface {

    MenuItem menuItem;
    ImageView ivBack;
    ImageView ivItemPicture;
    TextView tvItemName;
    TextView tvItemPrice;
    TextView tvItemDescription;
    TextView tvTotalCartPrice;
    TextView tvTotalItemPrice;
    Button btnAddItem;
    RecyclerView rvMenuItems;
    AddOnItemsRecyclerAdapter addOnItemsRecyclerAdapter;

    ProgressDialog progressDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_item);
        menuItem = (MenuItem) getIntent().getExtras().getSerializable("menuItem");

        ivItemPicture = findViewById(R.id.ivItemPicture);
        ivBack = findViewById(R.id.ivBack);
        tvItemName = findViewById(R.id.tvItemName);
        tvItemPrice = findViewById(R.id.tvItemPrice);
        tvItemDescription = findViewById(R.id.tvItemDescription);
        btnAddItem = findViewById(R.id.btnAddItem);
        tvTotalCartPrice = findViewById(R.id.tvTotalCartPrice);
        tvTotalItemPrice = findViewById(R.id.tvTotalItemPrice);
        rvMenuItems = findViewById(R.id.rvMenuItems);

        if (menuItem.itemPhotoUrl != null && !menuItem.itemPhotoUrl.equals("")) {
            Picasso.with(getApplicationContext()).load(menuItem.itemPhotoUrl).into(ivItemPicture);
        }

        addOnItemsRecyclerAdapter = new AddOnItemsRecyclerAdapter(getApplicationContext(), new ArrayList<AddOnItem>(), new OnAddOnItemSelectedInterface() {
            @Override
            public void onAddOnItemIncremented(int position, AddOnItem addOnItem) {
                addOnItem.menuItemId = menuItem.id;
                menuItem.addOnItems.add(addOnItem);

                // Sum up price of item price and all add on items
                int currentPrice = menuItem.price;
                for (AddOnItem item : menuItem.addOnItems) {
                    currentPrice += item.price;
                }
                // Set sum value
                NumberFormat n = NumberFormat.getCurrencyInstance(Locale.US);
                String s = n.format(currentPrice / 100.0);
                tvTotalItemPrice.setText("Total Price:  " + s);

            }

            @Override
            public void onAddOnItemDecremented(int position, AddOnItem addOnItem) {
                // We have to find the position of an addOn and then remove that item directly.
                int count = 0;
                for (AddOnItem addOnItem1 : menuItem.addOnItems) {
                    if (addOnItem.name.equals(addOnItem.name)) {
                        menuItem.addOnItems.remove(count);
                        break;
                    }
                    count++;
                }

                // Sum up price of item price and all add on items
                int currentPrice = menuItem.price;
                for (AddOnItem item : menuItem.addOnItems) {
                    currentPrice += item.price;
                }
                // Set sum value
                NumberFormat n = NumberFormat.getCurrencyInstance(Locale.US);
                String s = n.format(currentPrice / 100.0);
                tvTotalItemPrice.setText("Total Price:  " + s);
            }

        });

        rvMenuItems.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        rvMenuItems.setAdapter(addOnItemsRecyclerAdapter);

        // Set Item values
        NumberFormat n = NumberFormat.getCurrencyInstance(Locale.US);
        String s = n.format(menuItem.price / 100.0);

        tvItemName.setText(menuItem.name);
        tvTotalItemPrice.setText("Total Price:  " + s);
        tvItemPrice.setText(s);
        tvItemDescription.setText(menuItem.description);

        ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        btnAddItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                // Session is expired, show dialog
                new AlertDialog.Builder(MenuItemActivity.this)
                        .setMessage("Place Item Order?")
                        .setPositiveButton("Order Item", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {


                                // Show Progress Dialog as loading
                                progressDialog = new ProgressDialog(MenuItemActivity.this);
                                progressDialog.setMessage("Ordering.");
                                progressDialog.show();

                                menuItem.state = "NEW";

                                UserOrder.getInstance(getApplicationContext()).addItemToOrder(menuItem, MenuItemActivity.this);
                                OrderService.getInstance(getApplicationContext()).requestFoodForSession(new OnRequestFoodForSessionInterface() {
                                    @Override
                                    public void onRequestSent() {

                                        progressDialog.setMessage("Ordering..");

                                        User currentUser = CurrentUser.getInstance(getApplicationContext()).getCache();
                                        currentUser.currentSessionState = "PENDING_FOOD_ORDER";
                                        CurrentUser.getInstance(getApplication()).persist(currentUser, new UpdateUserToDatabaseInterface() {
                                            @Override
                                            public void onUserUpdated() {

                                                progressDialog.dismiss();
                                                Intent intent = new Intent(getApplicationContext(), RestaurantManagerActivity.class);
                                                startActivity(intent);
                                                finish();
                                            }

                                            @Override
                                            public void onError() {
                                                progressDialog.dismiss();
                                                Toast.makeText(MenuItemActivity.this, "Error persisting profile", Toast.LENGTH_SHORT).show();

                                            }
                                        });


                                    }

                                    @Override
                                    public void onError() {
                                        progressDialog.dismiss();
                                        Toast.makeText(MenuItemActivity.this, "Error API call: requestFoodForSession", Toast.LENGTH_SHORT).show();
                                    }
                                });

                            }
                        })
                        .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                            }
                        })
                        .setCancelable(false)
                        .create()
                        .show();


            }
        });

        s = n.format(UserOrder.getInstance(getApplicationContext()).getTotalCartPrice() / 100.0);
        tvTotalCartPrice.setText(s);


        MenuService.getInstance(getApplicationContext()).getItemAddOns(menuItem.restaurantId, menuItem.categoryId, menuItem.id, new FetchRestaurantItemAddOnsInterface() {
            @Override
            public void onDataFetched(AddOnItem addOnItem) {
                addOnItemsRecyclerAdapter.addRow(addOnItem);
            }

            @Override
            public void onError() {

            }
        });

    }

    @Override
    public void onItemAdded(MenuItem menuItem, int newTotalCartPrice) {
        NumberFormat n = NumberFormat.getCurrencyInstance(Locale.US);
        String s = n.format(newTotalCartPrice / 100.0);
        tvTotalCartPrice.setText(s);
    }
}
