package com.food.quibil.activities;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.food.quibil.R;
import com.food.quibil.data_layer.CurrentUser;
import com.food.quibil.interfaces.UpdateUserToDatabaseInterface;
import com.food.quibil.models.User;
import com.food.quibil.utils.App;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.rey.material.widget.Button;
import com.rey.material.widget.EditText;

import java.io.ByteArrayOutputStream;
import java.net.URL;

import de.hdodenhof.circleimageview.CircleImageView;

import static java.security.AccessController.getContext;

public class LoginSignupActivity extends AppCompatActivity {
    String TAG = "LoginSignupActivity";

    LinearLayout llAddPhoto;
    TextView tvOrText;
    TextView tvLoginText;
    EditText etFirstName;
    EditText etLastName;
    EditText etEmail;
    EditText etPassword;
    EditText etPassword2;
    EditText edtPhoneNumber;
    Button btnSignUp;
    TextView tvSignUp;
    TextView tvMessage1;
    TextView tvAddPicture;
    CircleImageView civProfilePicture;
    ProgressDialog progressDialog;
    byte[] pictureData;

    private FirebaseAuth mAuth;

    final String[] items = new String[3];


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
        setContentView(R.layout.activity_login_signup);
        final Typeface face = Typeface.createFromAsset(getAssets(), "fonts/OpenSans-Light.ttf");
        App.getmFirebaseAnalytics().setCurrentScreen(this, "LoginSignupActivity", null /* class override */);

        mAuth = FirebaseAuth.getInstance();

        civProfilePicture = findViewById(R.id.civProfilePicture);
        llAddPhoto = findViewById(R.id.llAddPhoto);
        tvSignUp = (TextView) findViewById(R.id.tvSignUp);
        tvMessage1 = (TextView) findViewById(R.id.tvMessage1);
        tvAddPicture = (TextView) findViewById(R.id.tvAddPicture);
        btnSignUp = (Button) findViewById(R.id.btnSignUp);
        edtPhoneNumber = (EditText) findViewById(R.id.edtPhoneNumber);
        etEmail = (EditText) findViewById(R.id.etEmail);
        etPassword = (EditText) findViewById(R.id.etPassword);
        etPassword2 = (EditText) findViewById(R.id.etPassword2);
        etFirstName = findViewById(R.id.etFirstName);
        etLastName = findViewById(R.id.etLastName);
        tvLoginText = (TextView) findViewById(R.id.tvLoginText);
        tvOrText = (TextView) findViewById(R.id.tvOrText);


        tvOrText.setTypeface(face);
        btnSignUp.setTypeface(face);
        tvLoginText.setTypeface(face);
        tvSignUp.setTypeface(face);
        tvMessage1.setTypeface(face);
        tvAddPicture.setTypeface(face);
        edtPhoneNumber.setTypeface(face);
        etPassword.setTypeface(face);
        etPassword2.setTypeface(face);
        etEmail.setTypeface(face);
        etFirstName.setTypeface(face);
        etLastName.setTypeface(face);

        tvLoginText.setText(Html.fromHtml("Already have an account? <b>Login</b>"));

        btnSignUp.setVisibility(View.GONE);


        civProfilePicture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (checkPermission()) {
                    takePhotoOrChoose();
                } else {
                    requestPermission();
                }

            }
        });

        etEmail.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {

                if (!edtPhoneNumber.getText().toString().equals("") && !editable.toString().equals("") && !etPassword.getText().toString().equals("") && !etPassword2.getText().toString().equals("")) {
                    btnSignUp.setVisibility(View.VISIBLE);
                    btnSignUp.animate().alpha(1.0f).setDuration(500).start();
                } else {
                    btnSignUp.animate().alpha(0.0f).setDuration(500).start();
                }
            }
        });

        etPassword2.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {

                if (!edtPhoneNumber.getText().toString().equals("") && !editable.toString().equals("") && !etPassword.getText().toString().equals("") && !etEmail.getText().toString().equals("") && !etFirstName.getText().toString().equals("") && !etLastName.getText().toString().equals("")) {
                    btnSignUp.setVisibility(View.VISIBLE);
                    btnSignUp.animate().alpha(1.0f).setDuration(500).start();
                } else {
                    btnSignUp.animate().alpha(0.0f).setDuration(500).start();
                }
            }
        });

        etPassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {

                if (!edtPhoneNumber.getText().toString().equals("") && !editable.toString().equals("") && !etEmail.getText().toString().equals("") && !etPassword2.getText().toString().equals("") && !etFirstName.getText().toString().equals("") && !etLastName.getText().toString().equals("")) {
                    btnSignUp.setVisibility(View.VISIBLE);
                    btnSignUp.animate().alpha(1.0f).setDuration(500).start();
                } else {
                    btnSignUp.animate().alpha(0.0f).setDuration(500).start();
                }
            }
        });
        edtPhoneNumber.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {

                if (!editable.toString().equals("") && !etEmail.getText().toString().equals("") && !etPassword2.getText().toString().equals("") && !etPassword.getText().toString().equals("") && !etFirstName.getText().toString().equals("") && !etLastName.getText().toString().equals("")) {
                    btnSignUp.setVisibility(View.VISIBLE);
                    btnSignUp.animate().alpha(1.0f).setDuration(500).start();
                } else {
                    btnSignUp.animate().alpha(0.0f).setDuration(500).start();
                }
            }
        });


        etFirstName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {

                if (!editable.toString().equals("") && !etEmail.getText().toString().equals("") && !etPassword2.getText().toString().equals("") && !etPassword.getText().toString().equals("") && !edtPhoneNumber.getText().toString().equals("") && !etLastName.getText().toString().equals("")) {
                    btnSignUp.setVisibility(View.VISIBLE);
                    btnSignUp.animate().alpha(1.0f).setDuration(500).start();
                } else {
                    btnSignUp.animate().alpha(0.0f).setDuration(500).start();
                }
            }
        });

        etLastName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {

                if (!editable.toString().equals("") && !etEmail.getText().toString().equals("") && !etPassword2.getText().toString().equals("") && !etPassword.getText().toString().equals("") && !etFirstName.getText().toString().equals("") && !edtPhoneNumber.getText().toString().equals("")) {
                    btnSignUp.setVisibility(View.VISIBLE);
                    btnSignUp.animate().alpha(1.0f).setDuration(500).start();
                } else {
                    btnSignUp.animate().alpha(0.0f).setDuration(500).start();
                }
            }
        });

        tvLoginText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                startActivity(intent);
                finish();
            }
        });


        btnSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                // Validate fields
                if (etPassword.getText().length() == 0) {
                    Toast.makeText(LoginSignupActivity.this, "Password should not be empty", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (etPassword2.getText().length() == 0) {
                    Toast.makeText(LoginSignupActivity.this, "Password should not be empty", Toast.LENGTH_SHORT).show();
                    return;
                }

                if (etEmail.getText().toString().isEmpty()) {
                    Toast.makeText(LoginSignupActivity.this, "Email should not be empty", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (edtPhoneNumber.getText().toString().isEmpty()) {
                    Toast.makeText(LoginSignupActivity.this, "Phone number should not be empty", Toast.LENGTH_SHORT).show();
                    return;
                }


                if (!etPassword.getText().toString().equals(etPassword2.getText().toString())) {
                    Toast.makeText(LoginSignupActivity.this, "Passwords don't match",
                            Toast.LENGTH_SHORT).show();
                    return;
                }

                progressDialog = new ProgressDialog(LoginSignupActivity.this);
                progressDialog.setMessage("Signing up");
                progressDialog.show();

                etEmail.setText(etEmail.getText().toString().replaceAll(" ", ""));

                mAuth.createUserWithEmailAndPassword(etEmail.getText().toString(), etPassword.getText().toString())
                        .addOnCompleteListener(LoginSignupActivity.this, new OnCompleteListener<AuthResult>() {
                            @Override
                            public void onComplete(@NonNull Task<AuthResult> task) {


                                if (task.isSuccessful()) {

                                    // Sign in success, update UI with the signed-in user's information
                                    Log.d("LoginSignUpActivity", "createUserWithEmail:success");
                                    final FirebaseUser user = mAuth.getCurrentUser();


                                    // If there's picture data, then upload picture
                                    if (pictureData != null) {
                                        FirebaseStorage storage = FirebaseStorage.getInstance();
                                        // Create a storage reference from our app
                                        StorageReference storageRef = storage.getReference();
                                        // Create a reference to profile picture
                                        StorageReference photoRef = storageRef.child("images/client_profile_image/" + user.getUid() + "/profile_image.jpeg");

                                        // Upload photo to firebase
                                        UploadTask uploadTask = photoRef.putBytes(pictureData);
                                        uploadTask.addOnFailureListener(new OnFailureListener() {
                                            @Override
                                            public void onFailure(@NonNull Exception exception) {
                                                // Handle unsuccessful uploads
                                                Log.e("onActivityResult", "Upload Faliure: " + exception.toString());
                                            }
                                        }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                                            @Override
                                            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                                                Log.d("onActivityResult", "Upload SUCCESS: ");

                                                // taskSnapshot.getMetadata() contains file metadata such as size, content-type, and download URL.
                                                Uri downloadUrl = taskSnapshot.getTask().getResult().getUploadSessionUri();

                                                final User currentUser = new User();
                                                currentUser.profilePictureURL = downloadUrl.toString();
                                                currentUser.uid = user.getUid();
                                                currentUser.firstName = etFirstName.getText().toString();
                                                currentUser.lastName = etLastName.getText().toString();
                                                currentUser.phoneNumber = edtPhoneNumber.getText().toString();
                                                currentUser.email = user.getEmail();

                                                CurrentUser.getInstance(getApplicationContext()).persist(currentUser, new UpdateUserToDatabaseInterface() {
                                                    @Override
                                                    public void onUserUpdated() {
                                                        // Remove Progress Dialog
                                                        progressDialog.dismiss();

                                                        Intent intent = new Intent(getApplicationContext(), ScanActivity.class);
                                                        startActivity(intent);
                                                        finish();


                                                    }

                                                    @Override
                                                    public void onError() {
                                                        // Remove Progress Dialog
                                                        progressDialog.dismiss();

                                                        Log.e(TAG, "Error saving user to database");
                                                    }
                                                });
                                                CurrentUser.getInstance(getApplicationContext()).cacheUser(currentUser);

                                            }
                                        });
                                    }

                                    // Otherwise save everything without profile picture
                                    else {


                                        final User currentUser = new User();
                                        currentUser.uid = user.getUid();
                                        currentUser.firstName = etFirstName.getText().toString();
                                        currentUser.lastName = etLastName.getText().toString();
                                        currentUser.phoneNumber = edtPhoneNumber.getText().toString();
                                        currentUser.email = user.getEmail();

                                        CurrentUser.getInstance(getApplicationContext()).persist(currentUser, new UpdateUserToDatabaseInterface() {
                                            @Override
                                            public void onUserUpdated() {

                                                // Remove Progress Dialog
                                                progressDialog.dismiss();

                                                CurrentUser.getInstance(getApplicationContext()).cacheUser(currentUser);

                                                Intent intent = new Intent(getApplicationContext(), ScanActivity.class);
                                                startActivity(intent);
                                                finish();
                                            }

                                            @Override
                                            public void onError() {

                                                // Remove Progress Dialog
                                                progressDialog.dismiss();

                                            }
                                        });
                                    }


                                } else {
                                    // If sign in fails, display a message to the user.
                                    Log.w("LoginSignUpActivity", "createUserWithEmail:failure", task.getException());
                                    Toast.makeText(LoginSignupActivity.this, task.getException().getMessage(),
                                            Toast.LENGTH_SHORT).show();
                                }

                            }
                        });

            }
        });
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {


        //Detects request codes
        if ((requestCode == 1888 || requestCode == 1889) && resultCode == Activity.RESULT_OK) {

            // Set image to bitmap
            try {

                // Get image bitmap and set to images
                Bitmap bitmap;
                if (requestCode == 1889) {
                    bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), data.getData());
                } else {
                    bitmap = (Bitmap) data.getExtras().get("data");
                }

                llAddPhoto.setVisibility(View.GONE);
                civProfilePicture.setImageBitmap(bitmap);

                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 80, baos);
                pictureData = baos.toByteArray();

                float aspectRatio = bitmap.getWidth() / (float) bitmap.getHeight();

                int height = 500;
                int width = Math.round(height * aspectRatio);

                bitmap = Bitmap.createScaledBitmap(
                        bitmap, width, height, false);

                final Handler handler = new Handler();
                final Bitmap finalBitmap = bitmap;
                Runnable runnable = new Runnable() {
                    public void run() {

                        URL img_value = null;
                        Bitmap mIcon1 = null;

                        try {
                            handler.post(new Runnable() {
                                public void run() {
                                    civProfilePicture.setImageBitmap(finalBitmap);
                                }
                            });

                        } catch (Exception e) {
                            Log.e("IOException", "onActivityResult");
                            e.printStackTrace();
                        }


                    }
                };
                new Thread(runnable).start();

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }


    private void takePhotoOrChoose() {


        new AlertDialog.Builder(LoginSignupActivity.this)
                .setMessage("Select Destination")
                .setPositiveButton("Camera", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        // Request to take a photo
                        Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                        startActivityForResult(cameraIntent, 1888);
                    }
                })
                .setNegativeButton("Gallery", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        Intent pickPhoto = new Intent(Intent.ACTION_PICK,
                                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                        startActivityForResult(pickPhoto, 1889);
                    }
                })
                .create()
                .show();


    }


    private boolean checkPermission() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED) {
            // Permission is not granted
            return false;
        }
        return true;
    }

    private void requestPermission() {

        ActivityCompat.requestPermissions(this,
                new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE},
                12345);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 12345:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(getApplicationContext(), "Permission Granted", Toast.LENGTH_SHORT).show();

                    takePhotoOrChoose();
                } else {
                    Toast.makeText(getApplicationContext(), "Permission Denied", Toast.LENGTH_SHORT).show();
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA)
                                != PackageManager.PERMISSION_GRANTED) {
                            showMessageOKCancel("You need to allow access permissions",
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                                requestPermission();
                                            }
                                        }
                                    });
                        }
                    }
                }
                break;
        }
    }

    private void showMessageOKCancel(String message, DialogInterface.OnClickListener okListener) {
        new AlertDialog.Builder(LoginSignupActivity.this)
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Cancel", null)
                .create()
                .show();
    }


}
