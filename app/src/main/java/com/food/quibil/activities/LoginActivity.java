package com.food.quibil.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.food.quibil.R;
import com.food.quibil.data_layer.CurrentUser;
import com.food.quibil.interfaces.UserServiceInterface;
import com.food.quibil.models.User;
import com.food.quibil.utils.App;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthException;
import com.google.firebase.auth.FirebaseUser;
import com.rey.material.widget.Button;
import com.rey.material.widget.EditText;

public class LoginActivity extends AppCompatActivity {

    EditText etEmail;
    EditText edtPassword;
    Button btnLogin;
    TextView tvLogin;


    FirebaseAuth mAuth;

    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
        setContentView(R.layout.activity_login);
        Typeface face = Typeface.createFromAsset(getAssets(), "fonts/OpenSans-Light.ttf");
        App.getmFirebaseAnalytics().setCurrentScreen(this, "LoginActivity", null /* class override */);

        mAuth = FirebaseAuth.getInstance();

        etEmail = (EditText) findViewById(R.id.etEmail);
        edtPassword = (EditText) findViewById(R.id.etPassword);
        btnLogin = (Button) findViewById(R.id.btnLogin);
        tvLogin = (TextView) findViewById(R.id.tvLogin);

        etEmail.setTypeface(face);
        edtPassword.setTypeface(face);
        tvLogin.setTypeface(face);
        btnLogin.setTypeface(face);
        btnLogin.setVisibility(View.GONE);

        etEmail.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {

                if (!editable.toString().equals("") && !edtPassword.getText().toString().equals("")) {
                    btnLogin.setVisibility(View.VISIBLE);
                    btnLogin.animate().alpha(1.0f).setDuration(500).start();
                } else {
                    btnLogin.animate().alpha(0.0f).setDuration(500).start();
                }

            }
        });

        edtPassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {

                if (!editable.toString().equals("") && !edtPassword.getText().toString().equals("") && !etEmail.getText().toString().equals("")) {
                    btnLogin.setVisibility(View.VISIBLE);
                    btnLogin.animate().alpha(1.0f).setDuration(500).start();
                } else {
                    btnLogin.animate().alpha(0.0f).setDuration(500).start();
                }

            }
        });

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                // Validate fields
                if(edtPassword.getText().length() == 0){
                    Toast.makeText(LoginActivity.this, "Password should not be empty", Toast.LENGTH_SHORT).show();
                }


                // Show Progress Dialog as loading
                progressDialog = new ProgressDialog(LoginActivity.this);
                progressDialog.setMessage("Logging in");
                progressDialog.show();

                etEmail.setText(etEmail.getText().toString().replaceAll(" ", ""));

                mAuth.signInWithEmailAndPassword(etEmail.getText().toString(), edtPassword.getText().toString())
                        .addOnCompleteListener(LoginActivity.this, new OnCompleteListener<AuthResult>() {
                            @Override
                            public void onComplete(@NonNull Task<AuthResult> task) {


                                if (task.isSuccessful()) {
                                    // Sign in success, update UI with the signed-in user's information
                                    Log.d("LoginActivity", "signInWithEmail:success");
                                    FirebaseUser user = mAuth.getCurrentUser();


                                    // Cache UID and Email
                                    User currentUser = new User();
                                    currentUser.uid = user.getUid();
                                    currentUser.email = user.getEmail();
                                    CurrentUser.getInstance(getApplicationContext()).cacheUser(currentUser);


                                    // Fetch user data and cache it
                                    CurrentUser.getInstance(getApplicationContext()).fetch(new UserServiceInterface() {
                                        @Override
                                        public void onDataFetched(User user) {
                                            Log.d("LoginActivity", "fetch:onDataFetched");

                                            CurrentUser.getInstance(getApplicationContext()).cacheUser(user);

                                            Intent intent = new Intent(LoginActivity.this, ScanActivity.class);
                                            startActivity(intent);
                                            finish();


                                        }

                                        @Override
                                        public void onUserDataNotFound() {
                                            // Remove Progress Dialog
                                            progressDialog.dismiss();

                                            Log.d("LoginActivity", "fetch:onUserDataNotFound");

                                            Intent intent = new Intent(LoginActivity.this, LoginSignupActivity.class);
                                            startActivity(intent);
                                            finish();
                                        }
                                    });


                                } else {
                                    // Remove Progress Dialog
                                    progressDialog.dismiss();

                                    // If sign in fails, display a message to the user.
                                    Log.w("LoginActivity", "signInWithEmail:failure", task.getException());
                                    if (task.getException() != null) {
                                        String errorCode = ((FirebaseAuthException) task.getException()).getErrorCode();
                                        switch (errorCode) {
                                            case ("ERROR_INVALID_EMAIL"):
                                                Toast.makeText(getApplicationContext(), "Email not found.",
                                                        Toast.LENGTH_SHORT).show();
                                                break;

                                            case ("ERROR_USER_NOT_FOUND"):
                                                Toast.makeText(getApplicationContext(), "Email not found.",
                                                        Toast.LENGTH_SHORT).show();
                                                break;
                                            case ("ERROR_WRONG_PASSWORD"):
                                                Toast.makeText(getApplicationContext(), "Email and Password combination does not work.",
                                                        Toast.LENGTH_SHORT).show();
                                                break;
                                            case "ERROR_INVALID_CUSTOM_TOKEN":
                                                Toast.makeText(getApplicationContext(), "The custom token format is incorrect. Please check the documentation.", Toast.LENGTH_LONG).show();
                                            case "ERROR_CUSTOM_TOKEN_MISMATCH":
                                                Toast.makeText(getApplicationContext(), "The custom token corresponds to a different audience.", Toast.LENGTH_LONG).show();
                                                break;
                                            case "ERROR_INVALID_CREDENTIAL":
                                                Toast.makeText(getApplicationContext(), "The supplied auth credential is malformed or has expired.", Toast.LENGTH_LONG).show();
                                                break;
                                            case "ERROR_USER_MISMATCH":
                                                Toast.makeText(getApplicationContext(), "The supplied credentials do not correspond to the previously signed in user.", Toast.LENGTH_LONG).show();
                                                break;
                                            case "ERROR_REQUIRES_RECENT_LOGIN":
                                                Toast.makeText(getApplicationContext(), "This operation is sensitive and requires recent authentication. Log in again before retrying this request.", Toast.LENGTH_LONG).show();
                                                break;
                                            case "ERROR_ACCOUNT_EXISTS_WITH_DIFFERENT_CREDENTIAL":
                                                Toast.makeText(getApplicationContext(), "An account already exists with the same email address but different sign-in credentials. Sign in using a provider associated with this email address.", Toast.LENGTH_LONG).show();
                                                break;
                                            case "ERROR_EMAIL_ALREADY_IN_USE":
                                                Toast.makeText(getApplicationContext(), "The email address is already in use by another account.   ", Toast.LENGTH_LONG).show();
                                                break;
                                            case "ERROR_CREDENTIAL_ALREADY_IN_USE":
                                                Toast.makeText(getApplicationContext(), "This credential is already associated with a different user account.", Toast.LENGTH_LONG).show();
                                                break;
                                            case "ERROR_USER_DISABLED":
                                                Toast.makeText(getApplicationContext(), "The user account has been disabled by an administrator.", Toast.LENGTH_LONG).show();
                                                break;
                                            case "ERROR_USER_TOKEN_EXPIRED":
                                                Toast.makeText(getApplicationContext(), "The user\\'s credential is no longer valid. The user must sign in again.", Toast.LENGTH_LONG).show();
                                                break;
                                            case "ERROR_INVALID_USER_TOKEN":
                                                Toast.makeText(getApplicationContext(), "The user\\'s credential is no longer valid. The user must sign in again.", Toast.LENGTH_LONG).show();
                                                break;
                                            case "ERROR_OPERATION_NOT_ALLOWED":
                                                Toast.makeText(getApplicationContext(), "This operation is not allowed. You must enable this service in the console.", Toast.LENGTH_LONG).show();
                                                break;
                                            case "ERROR_WEAK_PASSWORD":
                                                Toast.makeText(getApplicationContext(), "The given password is invalid.", Toast.LENGTH_LONG).show();
                                                edtPassword.setError("The password is invalid it must 6 characters at least");
                                                edtPassword.requestFocus();
                                                break;

                                            default:
                                                Toast.makeText(getApplicationContext(), "Authentication failed. Please check internet connection.",
                                                        Toast.LENGTH_SHORT).show();
                                                break;
                                        }
                                    }

                                }
                            }
                        });
            }
        });

    }

    @Override
    public void onBackPressed() {

        Intent intent = new Intent(getApplicationContext(), LoginSignupActivity.class);
        startActivity(intent);
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
        finish();

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if(progressDialog != null && progressDialog.isShowing()){
            progressDialog.dismiss();
        }
    }
}
