package com.food.quibil.activities;

import android.animation.ObjectAnimator;
import android.animation.PropertyValuesHolder;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.food.quibil.R;
import com.food.quibil.data_layer.UserOrder;
import com.food.quibil.models.Order;
import com.food.quibil.models.Session;
import com.food.quibil.ui.features.finishandpay.FinishAndPayActivity;
import com.food.quibil.ui.features.menu.OrderActivity;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class IntermediateOrderActivity extends AppCompatActivity {
    private String TAG = "IntermediateOrderActivity";

    ImageView ivFood;
    TextView tvState;
    TextView tvFinishedEating;
    Button btnFinishAndPay;
    ValueEventListener postListener;

    Session session;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_intermediate_order);

        ivFood = findViewById(R.id.ivFood);
        btnFinishAndPay = findViewById(R.id.btnFinishAndPay);
        tvState = findViewById(R.id.tvState);
        tvFinishedEating = findViewById(R.id.tvFinishedEating);


        btnFinishAndPay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (session != null && session.state != null) {
                    if (!session.state.equals("DELIVERED")) {
                        Toast.makeText(IntermediateOrderActivity.this, "Your food hasn't been delivered yet!", Toast.LENGTH_SHORT).show();
                    } else {
                        Intent intent = new Intent(getApplicationContext(), FinishAndPayActivity.class);
                        startActivity(intent);
                    }
                }
            }
        });

        // Pulsate Food Icon
        ObjectAnimator scaleDown = ObjectAnimator.ofPropertyValuesHolder(
                ivFood,
                PropertyValuesHolder.ofFloat("scaleX", 1.2f),
                PropertyValuesHolder.ofFloat("scaleY", 1.2f));
        scaleDown.setDuration(1000);
        scaleDown.setRepeatCount(ObjectAnimator.INFINITE);
        scaleDown.setRepeatMode(ObjectAnimator.REVERSE);
        scaleDown.start();

    }

    @Override
    protected void onStart() {
        super.onStart();


        postListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                session = dataSnapshot.getValue(Session.class);

                // Cache
                UserOrder.getInstance(getApplicationContext()).persistSessionId(session.id);
                UserOrder.getInstance(getApplicationContext()).persistSessionState(session.state);
                UserOrder.getInstance(getApplicationContext()).cacheRestaurantId(session.restaurantId);

                Order order = new Order();
                order.orderedItems = session.orders;
                UserOrder.getInstance(getApplicationContext()).setOrder(order);

                if (session.state != null) {
                    if (session.state.equals("DELIVERED")) {
                        tvState.setText("Your food has been delivered!");
                        tvFinishedEating.setVisibility(View.VISIBLE);
                        btnFinishAndPay.setVisibility(View.VISIBLE);
                    } else if(session.state.equals("PENDING_FOOD_ORDER")){
                        tvFinishedEating.setVisibility(View.GONE);
                        btnFinishAndPay.setVisibility(View.GONE);
                    } else if(session.state.equals("CHECKED_IN")){
                        Intent intent = new Intent(getApplicationContext(), OrderActivity.class);
                        startActivity(intent);
                    }
                    else {

                    }
                }
                // ...
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                // Getting Post failed, log a message
                Log.e(TAG, "postListener:onCancelled", databaseError.toException());
            }
        };


        String currentSessionId = UserOrder.getInstance(getApplicationContext()).getSessionId();
        if (currentSessionId != null && !currentSessionId.equals("")) {
            FirebaseDatabase.getInstance().getReference()
                    .child("order_sessions")
                    .child(UserOrder.getInstance(getApplicationContext()).getSessionId())
                    .addValueEventListener(postListener);
        } else {

            // Session is expired, show dialog
            new AlertDialog.Builder(IntermediateOrderActivity.this)
                    .setMessage("Session Expired")
                    .setPositiveButton("Exit", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            // Go back to scan activity
                            Intent cameraIntent = new Intent(getApplicationContext(), ScanActivity.class);
                            startActivity(cameraIntent);
                            finish();
                        }
                    })
                    .setCancelable(false)
                    .create()
                    .show();
        }

    }


    @Override
    protected void onStop() {
        super.onStop();

        FirebaseDatabase.getInstance().getReference().removeEventListener(postListener);
    }
}
