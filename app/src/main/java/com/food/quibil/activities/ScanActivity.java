package com.food.quibil.activities;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.food.quibil.R;
import com.food.quibil.interfaces.FetchQRCode;
import com.food.quibil.models.QRCode;
import com.food.quibil.network_layer.OrderService;
import com.food.quibil.ui.features.sessionhistory.SessionHistoryActivity;
import com.google.firebase.auth.FirebaseAuth;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.Result;

import java.util.ArrayList;

import me.dm7.barcodescanner.zxing.ZXingScannerView;


public class ScanActivity extends AppCompatActivity implements ZXingScannerView.ResultHandler {
    String TAG = "ScanActivity";

    TextView tvQRMessage;
    TextView tvLogout;
    TextView tvProfile;
    TextView tvHistory;
    ZXingScannerView mScannerView;
    ProgressDialog progressDialog;

    private static final int PERMISSION_CAMERA = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scan);
        ArrayList<BarcodeFormat> formats = new ArrayList<>();
        formats.add(BarcodeFormat.QR_CODE);

        mScannerView = (ZXingScannerView) findViewById(R.id.mScannerView);
        mScannerView.setFormats(formats);

        tvHistory = findViewById(R.id.tvHistory);
        tvLogout = findViewById(R.id.tvLogout);
        tvQRMessage = (TextView) findViewById(R.id.tvQRMessage);
        tvProfile = findViewById(R.id.tvProfile);


        tvLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                // Session is expired, show dialog
                new AlertDialog.Builder(ScanActivity.this)
                        .setMessage("Are you sure you want to log out?")
                        .setPositiveButton("Log Out", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {

                                FirebaseAuth.getInstance().signOut();
                                Intent intent = new Intent(getApplicationContext(), SplashScreenActivity.class);
                                startActivity(intent);
                                finish();
                            }
                        })
                        .setNegativeButton("Nevermind", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                            }
                        })
                        .setCancelable(false)
                        .create()
                        .show();


            }
        });

        tvProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), ProfileActivity.class);
                startActivity(intent);
            }
        });

        tvHistory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), SessionHistoryActivity.class);
                startActivity(intent);
            }
        });

    }

    @Override
    public void onResume() {
        super.onResume();

        if (arePermissionsGranted()) {
            mScannerView.setResultHandler(this);
            mScannerView.startCamera();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        mScannerView.stopCamera(); // Stop camera on pause
    }


    public boolean arePermissionsGranted() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (checkSelfPermission(Manifest.permission.CAMERA)
                    == PackageManager.PERMISSION_GRANTED) {
                Log.v(TAG, "Permission is granted");
                return true;
            } else {

                Log.v(TAG, "Permission is revoked");
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA}, PERMISSION_CAMERA);
                return false;
            }
        } else { //permission is automatically granted on sdk<23 upon installation
            Log.v(TAG, "Permission is granted");
            return true;
        }
    }

    @Override
    public void handleResult(Result rawResult) {
        // Do something with the result here
        Log.v("ScanActivity", rawResult.getText()); // Prints scan results

        progressDialog = new ProgressDialog(ScanActivity.this);
        progressDialog.setMessage("Scanning QR Code");
        progressDialog.show();
        progressDialog.setCancelable(false);


        mScannerView.setResultHandler(null);
        mScannerView.stopCamera();

        final String scanString = rawResult.getText();
        final String qrTAG = scanString.substring(0,7);


        if (!qrTAG.equals("QUIBIL:")) {
            Snackbar.make(findViewById(android.R.id.content), "This QR Code is not acceptable", Snackbar.LENGTH_LONG)
                    .setAction("Okay", new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {

                        }
                    })
                    .setActionTextColor(getResources().getColor(R.color.colorAccent ))
                    .show();

            progressDialog.dismiss();
            mScannerView.setResultHandler(ScanActivity.this);
            mScannerView.startCamera();
        } else {

            final String qrCodeId = scanString.substring(7).trim();
            OrderService.getInstance(getApplicationContext()).getRestaurantdIdFromQRCode(qrCodeId, new FetchQRCode() {
                @Override
                public void onDataFetched(final QRCode qrCode) {

                    progressDialog.dismiss();

                    if (qrCode == null || qrCode.restaurantId.equals("")) {

                        Log.e(TAG, "QR Code is null, ID: " + qrCodeId);
                        Snackbar.make(findViewById(android.R.id.content), "This QR Code is not registered to any restaurant", Snackbar.LENGTH_LONG)
                                .setAction("Okay", new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {

                                    }
                                })
                                .setActionTextColor(getResources().getColor(R.color.colorAccent))
                                .show();

                        mScannerView.setResultHandler(ScanActivity.this);
                        mScannerView.startCamera();
                    } else {
                        Intent intent = new Intent(getApplicationContext(), CheckPaymentStatus.class);
                        intent.putExtra("qrCode", qrCode);
                        startActivity(intent);
                        finish();
                    }

//                OrderService.getInstance(getApplicationContext()).checkinUserToRestaurant(qrCode.restaurantId, qrCode.tableId, new OnCheckinUserToRestaurantInterface() {
//                    @Override
//                    public void onUserCheckedIn(String sessionId) {
//                        UserOrder.getInstance(getApplicationContext()).persistSessionId(sessionId);
//                        UserOrder.getInstance(getApplicationContext()).cacheRestaurantId(qrCode.restaurantId);
//
//                        // Cache user and update user in database
//                        User currentUser = CurrentUser.getInstance(getApplicationContext()).getCache();
//                        currentUser.currentSession = sessionId;
//                        currentUser.currentSessionState = "CHECKED_IN";
//                        CurrentUser.getInstance(getApplicationContext()).persist(currentUser, new UpdateUserToDatabaseInterface() {
//                            @Override
//                            public void onUserUpdated() {
//                                progressDialog.dismiss();
//                                Intent intent = new Intent(getApplicationContext(), CheckPaymentStatus.class);
//                                startActivity(intent);
//                                finish();
//                            }
//
//                            @Override
//                            public void onError() {
//                                progressDialog.dismiss();
//                            }
//                        });
//
//
//                    }
//
//                    @Override
//                    public void onError() {
//                        progressDialog.dismiss();
//                        mScannerView.setResultHandler(ScanActivity.this);
//                        mScannerView.startCamera();
//                    }
//                });

                }

                @Override
                public void onError() {
                    mScannerView.setResultHandler(ScanActivity.this);
                    mScannerView.startCamera();
                }
            });

        }

        //tvQRMessage.setText(rawResult.getText());

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        //Checking the request code of our request
        if (requestCode == PERMISSION_CAMERA) {

            //If permission is granted
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                //Displaying a toast
                Toast.makeText(this, "Permission Granted!", Toast.LENGTH_LONG).show();

                mScannerView.setResultHandler(this);
                mScannerView.startCamera();

            } else {
                //Displaying another toast if permission is not granted
                Toast.makeText(this, "Oops you just denied the permission", Toast.LENGTH_LONG).show();
            }
        }

    }
}
