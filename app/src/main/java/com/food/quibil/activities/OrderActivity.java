package com.food.quibil.activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.food.quibil.R;
import com.food.quibil.data_layer.UserOrder;
import com.food.quibil.fragments.MenuItemsListFragment;
import com.food.quibil.interfaces.FetchRestaurantMenuCategoriesInterface;
import com.food.quibil.models.MenuCategory;
import com.food.quibil.network_layer.MenuService;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class OrderActivity extends AppCompatActivity {

    Button btnMyItems;
    TabLayout tlTabs;
    ViewPager viewPager;
    ViewPagerAdapter adapter;
    TextView tvTotalCartPrice;
    ImageView ivClose;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order);

        // Check that restaurant id is not empty
        if(!UserOrder.getInstance(getApplicationContext()).getRestaurantId().equals("")) {
            MenuService.getInstance(getApplicationContext()).getMenuCategories(UserOrder.getInstance(getApplicationContext()).getRestaurantId(), new FetchRestaurantMenuCategoriesInterface() {
                @Override
                public void onDataFetched(MenuCategory menuCategory) {
                    adapter.addFragment(MenuItemsListFragment.newInstance(menuCategory), menuCategory.id);
                    tlTabs.addTab(tlTabs.newTab().setText(menuCategory.name));

                    tlTabs.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
                        @Override
                        public void onTabSelected(TabLayout.Tab tab) {
                            viewPager.setCurrentItem(tab.getPosition());
                        }

                        @Override
                        public void onTabUnselected(TabLayout.Tab tab) {

                        }

                        @Override
                        public void onTabReselected(TabLayout.Tab tab) {

                        }
                    });
                }

                @Override
                public void onError() {

                }
            });
        }
        // Otherwise go back, something went wrong
        else {
            // Session is expired, show dialog
            new AlertDialog.Builder(OrderActivity.this)
                    .setMessage("Session Expired")
                    .setPositiveButton("Exit", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {

                            // Update and cache user states
                            UserOrder.getInstance(getApplicationContext()).persistSessionId("");
                            UserOrder.getInstance(getApplicationContext()).persistSessionState("CHECKED_OUT");
                            UserOrder.getInstance(getApplicationContext()).cacheRestaurantId("");


                            // Go back to scan activity
                            Intent cameraIntent = new Intent(getApplicationContext(), ScanActivity.class);
                            startActivity(cameraIntent);
                            finish();
                        }
                    })
                    .setCancelable(false)
                    .create()
                    .show();
        }

        viewPager = findViewById(R.id.viewPager);
        tlTabs = findViewById(R.id.tlTabs);
        tvTotalCartPrice = findViewById(R.id.tvTotalCartPrice);
        ivClose = findViewById(R.id.ivClose);
        btnMyItems = findViewById(R.id.btnMyItems);

        btnMyItems.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), FinishAndPayActivity.class);
                startActivity(intent);
            }
        });

        ivClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });



        setupViewPager();
    }

    @Override
    protected void onStart() {
        super.onStart();

        // Format and set total cart price
        NumberFormat n = NumberFormat.getCurrencyInstance(Locale.US);
        String s = n.format(UserOrder.getInstance(getApplicationContext()).getTotalCartPrice() / 100.0);
        tvTotalCartPrice.setText(s);

        if(UserOrder.getInstance(getApplicationContext()).getTotalCartPrice() > 0){
            btnMyItems.setVisibility(View.VISIBLE);
            btnMyItems.setText("My Items ("+UserOrder.getInstance(getApplicationContext()).getTotalNumberOfItemsInCard()+")");
        } else {
            btnMyItems.setVisibility(View.GONE);
        }


    }

    private void setupViewPager() {
        adapter = new ViewPagerAdapter(getSupportFragmentManager());

        viewPager.setAdapter(adapter);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tlTabs));

        tlTabs.setTabGravity(TabLayout.GRAVITY_FILL);

    }







    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();
        private boolean doNotifyDataSetChangedOnce = false;

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            if (doNotifyDataSetChangedOnce) {
                doNotifyDataSetChangedOnce = false;
                notifyDataSetChanged();
            }

            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            doNotifyDataSetChangedOnce = true;

            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }



        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }
}
