package com.food.quibil.activities;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.widget.Toast;

import com.food.quibil.R;
import com.food.quibil.data_layer.CurrentUser;
import com.food.quibil.data_layer.UserOrder;
import com.food.quibil.interfaces.UserServiceInterface;
import com.food.quibil.models.User;
import com.food.quibil.ui.features.restaurantmanager.RestaurantManagerActivity;
import com.food.quibil.utils.App;
import com.google.firebase.auth.FirebaseAuth;


public class SplashScreenActivity extends Activity implements ActivityCompat.OnRequestPermissionsResultCallback {
    String TAG = "SplashScreenActivity";

    private FirebaseAuth mAuth;
    int REQUEST_WRITE_STORAGE = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
        App.getmFirebaseAnalytics().setCurrentScreen(this, TAG, null /* class override */);


    }

    @Override
    protected void onStart() {
        super.onStart();



        mAuth = FirebaseAuth.getInstance();

        if (mAuth.getCurrentUser() == null) {
            Intent intent = new Intent(getApplicationContext(), LoginSignupActivity.class);
            startActivity(intent);
            finish();
        } else {
            CurrentUser.getInstance(getApplicationContext()).fetch(new UserServiceInterface() {
                @Override
                public void onDataFetched(User user) {
                    Log.d(TAG, "fetch:onDataFetched");

                    // Cache user and session id in order
                    CurrentUser.getInstance(getApplicationContext()).cacheUser(user);
                    UserOrder.getInstance(getApplicationContext()).persistSessionState(user.currentSessionState);
                    UserOrder.getInstance(getApplicationContext()).persistSessionId(user.currentSession);
                    final User currentUser = CurrentUser.getInstance(getApplicationContext()).getCache();

                    // This supersedes all other cases
                    if(currentUser.currentSession.equals("")){
                        Intent intent = new Intent(SplashScreenActivity.this, ScanActivity.class);
                        startActivity(intent);
                        finish();
                    }

                    if(currentUser.currentSessionState.equals("CHECKED_IN")){
                        Intent intent = new Intent(SplashScreenActivity.this, RestaurantManagerActivity.class);
                        startActivity(intent);
                        finish();
                    } else if (currentUser.currentSessionState.equals("PENDING_FOOD_ORDER") || currentUser.currentSessionState.equals("DELIVERED")){
                        Intent intent = new Intent(SplashScreenActivity.this, RestaurantManagerActivity.class);
                        startActivity(intent);
                        finish();
                    }
                    // If no pending session then go to scan activity
                    else if (currentUser.currentSessionState.equals("CHECKED_OUT")){
                        Intent intent = new Intent(SplashScreenActivity.this, ScanActivity.class);
                        startActivity(intent);
                        finish();
                    }


                }

                @Override
                public void onUserDataNotFound() {

                    Log.d(TAG, "fetch:onUserDataNotFound");
                    Intent intent = new Intent(SplashScreenActivity.this, ScanActivity.class);
                    startActivity(intent);
                    finish();
                }
            });
        }


    }

    public boolean arePermissionsGranted() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED) {
                Log.v(TAG, "Permission is granted");
                return true;
            } else {

                Log.v(TAG, "Permission is revoked");
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_WRITE_STORAGE);
                return false;
            }
        } else { //permission is automatically granted on sdk<23 upon installation
            Log.v(TAG, "Permission is granted");
            return true;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        //Checking the request code of our request
        if (requestCode == REQUEST_WRITE_STORAGE) {

            //If permission is granted
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                //Displaying a toast
                Toast.makeText(this, "Permission Granted!", Toast.LENGTH_LONG).show();


                if (mAuth.getCurrentUser() == null) {
                    Intent intent = new Intent(getApplicationContext(), LoginSignupActivity.class);
                    startActivity(intent);
                    finish();

                } else {
                    Intent intent = new Intent(getApplicationContext(), ScanActivity.class);
                    startActivity(intent);
                    finish();
                }


            } else {
                //Displaying another toast if permission is not granted
                Toast.makeText(this, "Oops you just denied the permission", Toast.LENGTH_LONG).show();
            }
        }

    }
}
