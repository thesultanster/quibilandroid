package com.food.quibil.activities

import android.app.Activity
import android.app.ProgressDialog
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import com.braintreepayments.api.dropin.DropInActivity
import com.braintreepayments.api.dropin.DropInRequest
import com.braintreepayments.api.dropin.DropInResult
import com.food.quibil.R
import com.food.quibil.data_layer.CurrentUser
import com.food.quibil.data_layer.UserOrder
import com.food.quibil.interfaces.GenerateBraintreeClientTokenInterface
import com.food.quibil.interfaces.OnCheckinUserToRestaurantInterface
import com.food.quibil.interfaces.UpdateUserToDatabaseInterface
import com.food.quibil.models.Order
import com.food.quibil.models.QRCode
import com.food.quibil.network_layer.AuthService
import com.food.quibil.network_layer.OrderService
import com.food.quibil.ui.features.restaurantmanager.RestaurantManagerActivity
import com.food.quibil.utils.AppState
import kotlinx.android.synthetic.main.activity_check_payment_status.*

class CheckPaymentStatus : AppCompatActivity() {
    internal var TAG = "CheckPaymentStatus"
    internal var BT_CODE = 12345
    lateinit var progressDialog: ProgressDialog
    lateinit var qrCode: QRCode


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_check_payment_status)

        qrCode = intent.extras["qrCode"] as QRCode

        progressDialog = ProgressDialog(this)
        progressDialog.setMessage("Setting Up Card Details")
        progressDialog.show()
        progressDialog.setCancelable(false)

        btnCancel.setOnClickListener { finish() }
        btnOkay.setOnClickListener { openBraintreePaymentFlow() }

        openBraintreePaymentFlow()


    }

    fun openBraintreePaymentFlow(){
        AuthService.getInstance(applicationContext).generateBraintreeClientToken(object : GenerateBraintreeClientTokenInterface {
            override fun onClientTokenCreated(clientToken: String) {
                AppState.getInstance(applicationContext).cacheBraintreeClientToken(clientToken)

                val dropInRequest = DropInRequest()
                        .clientToken(clientToken)
                startActivityForResult(dropInRequest.getIntent(applicationContext), BT_CODE)

            }

            override fun onError() {
                progressDialog.dismiss()
            }
        })
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == BT_CODE) {

            if (resultCode == Activity.RESULT_OK) {
                val result = data!!.getParcelableExtra<DropInResult>(DropInResult.EXTRA_DROP_IN_RESULT)

                result.paymentMethodNonce!!.nonce

                progressDialog.setMessage("Checking In To Restaurant")

                OrderService.getInstance(applicationContext).checkinUserToRestaurant(qrCode.restaurantId, qrCode.tableId, object : OnCheckinUserToRestaurantInterface {
                    override fun onUserCheckedIn(sessionId: String) {
                        UserOrder.getInstance(applicationContext).persistSessionId(sessionId)
                        UserOrder.getInstance(applicationContext).cacheRestaurantId(qrCode.restaurantId)

                        // Cache user and update user in database
                        val currentUser = CurrentUser.getInstance(applicationContext).cache
                        currentUser.currentSession = sessionId
                        currentUser.currentSessionState = "CHECKED_IN"
                        CurrentUser.getInstance(applicationContext).persist(currentUser, object : UpdateUserToDatabaseInterface {
                            override fun onUserUpdated() {

                                UserOrder.getInstance(applicationContext).order = Order()
                                progressDialog.dismiss()
                                val intent = Intent(applicationContext, RestaurantManagerActivity::class.java)
                                startActivity(intent)
                                finish()
                            }

                            override fun onError() {
                                progressDialog.dismiss()
                                Log.e(TAG, "ERROR PERSISTING USER DURING AFTER CHECKIN")
                            }
                        })


                    }

                    override fun onError() {
                        progressDialog.dismiss()
                        Toast.makeText(this@CheckPaymentStatus, "Error Checking You In", Toast.LENGTH_SHORT).show()

                    }
                })

                // use the result to update your UI and send the payment method nonce to your server
            } else if (resultCode == Activity.RESULT_CANCELED) {
                progressDialog.dismiss()

                // the user canceled
            } else {
                // handle errors here, an exception may be available in
                val error = data!!.getSerializableExtra(DropInActivity.EXTRA_ERROR) as Exception
                Log.e(TAG, error.localizedMessage)
                progressDialog.dismiss()
                Toast.makeText(this, "Error Processing", Toast.LENGTH_SHORT).show()

            }
        }
    }
}
