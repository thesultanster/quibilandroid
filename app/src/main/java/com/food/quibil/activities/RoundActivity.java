package com.food.quibil.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.food.quibil.R;
import com.food.quibil.adapters.RoundItemsRecyclerAdapter;
import com.food.quibil.data_layer.UserOrder;
import com.food.quibil.interfaces.RoundItemInterface;
import com.food.quibil.interfaces.RoundItemRowType;
import com.food.quibil.models.AddOnItem;
import com.food.quibil.models.OrderedItem;

public class RoundActivity extends AppCompatActivity {


    Button btnPlaceOrder;
    ImageView ivBack;
    RecyclerView rvMenuItems;
    RoundItemsRecyclerAdapter roundItemsRecyclerAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_round);

        btnPlaceOrder = findViewById(R.id.btnPlaceOrder);
        ivBack = findViewById(R.id.ivBack);
        rvMenuItems = (RecyclerView) findViewById(R.id.rvMenuItems);

        roundItemsRecyclerAdapter = new RoundItemsRecyclerAdapter(getApplicationContext(), new RoundItemInterface() {
            @Override
            public void onUserWantsToRemoveItem(int position, RoundItemRowType type) {
                if(type instanceof OrderedItem){
                    UserOrder.getInstance(getApplicationContext()).removeItemFromOrder( (OrderedItem) type);
                } else if (type instanceof AddOnItem){
                    UserOrder.getInstance(getApplicationContext()).removeAddOnItemFromMenuItem((AddOnItem) type);
                }
            }
        });

        rvMenuItems.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        rvMenuItems.setAdapter(roundItemsRecyclerAdapter);
        roundItemsRecyclerAdapter.setOrderedItems(UserOrder.getInstance(getApplicationContext()).getOrder().orderedItems);

        ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        btnPlaceOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

    }


}
