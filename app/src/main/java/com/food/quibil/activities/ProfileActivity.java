package com.food.quibil.activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.ColorFilter;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.food.quibil.R;
import com.food.quibil.data_layer.CurrentUser;
import com.food.quibil.interfaces.UpdateUserToDatabaseInterface;
import com.food.quibil.models.User;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.rey.material.widget.EditText;
import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.net.URL;

import de.hdodenhof.circleimageview.CircleImageView;

public class ProfileActivity extends AppCompatActivity {
    String TAG = "ProfileActivity";

    TextView tvFirstName;
    TextView tvPhoneNumber;
    TextView tvLastName;
    TextView tvChangeProfilePhoto;
    EditText edtFirstName;
    EditText edtPhoneNumber;
    EditText edtLastName;
    TextView tvEdit;
    CircleImageView civProfilePicture;

    boolean isEditOn = false;

    Typeface openFace;
    User currentUser;
    byte[] pictureData;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        openFace = Typeface.createFromAsset(getAssets(), "fonts/OpenSans-Light.ttf");
        currentUser = CurrentUser.getInstance(getApplicationContext()).getCache();

        tvFirstName = findViewById(R.id.tvFirstName);
        tvPhoneNumber = findViewById(R.id.tvPhoneNumber);
        tvEdit = findViewById(R.id.tvEdit);
        tvLastName = findViewById(R.id.tvLastName);
        tvChangeProfilePhoto = findViewById(R.id.tvChangeProfilePhoto);
        edtFirstName = findViewById(R.id.edtFirstName);
        edtPhoneNumber = findViewById(R.id.edtPhoneNumber);
        edtLastName = findViewById(R.id.edtLastName);
        civProfilePicture = findViewById(R.id.civProfilePicture);

        tvChangeProfilePhoto.setVisibility(View.GONE);

        tvChangeProfilePhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                takePhotoOrChoose();

            }
        });
        civProfilePicture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                takePhotoOrChoose();

            }
        });
        if (currentUser.profilePictureURL != null && !currentUser.profilePictureURL.equals("")) {
            Picasso.with(getApplicationContext()).load(currentUser.profilePictureURL).into(civProfilePicture);
        } else {
            tvChangeProfilePhoto.setVisibility(View.GONE);
        }


        if (currentUser.firstName != null && !currentUser.firstName.equals("")) {
            tvFirstName.setText(currentUser.firstName);
        }
        if (currentUser.phoneNumber != null && !currentUser.phoneNumber.equals("")) {
            tvPhoneNumber.setText(currentUser.phoneNumber);
        }
        if (currentUser.lastName != null && !currentUser.lastName.equals("")) {
            tvLastName.setText(currentUser.lastName);
        }

        edtFirstName.setText(currentUser.firstName);
        edtPhoneNumber.setText(currentUser.phoneNumber);
        edtLastName.setText(currentUser.lastName);

        tvEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (isEditOn) {
                    isEditOn = false;
                    tvEdit.setText("EDIT");


                    tvFirstName.setText(edtFirstName.getText().toString());
                    tvPhoneNumber.setText(edtPhoneNumber.getText().toString());
                    tvLastName.setText(edtLastName.getText().toString());

                    currentUser = CurrentUser.getInstance(getApplicationContext()).getCache();
                    currentUser.firstName = edtFirstName.getText().toString();
                    currentUser.lastName = edtLastName.getText().toString();
                    currentUser.phoneNumber = edtPhoneNumber.getText().toString();
                    CurrentUser.getInstance(getApplicationContext()).cacheUser(currentUser);
                    CurrentUser.getInstance(getApplicationContext()).persist(currentUser, new UpdateUserToDatabaseInterface() {
                        @Override
                        public void onUserUpdated() {

                        }

                        @Override
                        public void onError() {

                        }
                    });

                    tvFirstName.setVisibility(View.VISIBLE);
                    tvPhoneNumber.setVisibility(View.VISIBLE);
                    tvLastName.setVisibility(View.VISIBLE);

                    edtFirstName.setVisibility(View.GONE);
                    edtPhoneNumber.setVisibility(View.GONE);
                    edtLastName.setVisibility(View.GONE);
                    tvChangeProfilePhoto.setVisibility(View.GONE);

                } else {
                    isEditOn = true;
                    tvEdit.setText("SAVE");

                    tvFirstName.setVisibility(View.GONE);
                    tvPhoneNumber.setVisibility(View.GONE);
                    tvLastName.setVisibility(View.GONE);

                    edtFirstName.setVisibility(View.VISIBLE);
                    edtPhoneNumber.setVisibility(View.VISIBLE);
                    edtLastName.setVisibility(View.VISIBLE);
                    tvChangeProfilePhoto.setVisibility(View.VISIBLE);

                }


            }
        });


    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {


        //Detects request codes
        if ((requestCode == 1888 || requestCode == 1889) && resultCode == Activity.RESULT_OK) {

            // Set image to bitmap
            try {

                // Get image bitmap and set to images
                Bitmap bitmap;
                if (requestCode == 1889) {
                    bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), data.getData());
                } else {
                    bitmap = (Bitmap) data.getExtras().get("data");
                }

                tvChangeProfilePhoto.setVisibility(View.GONE);
                civProfilePicture.setImageBitmap(bitmap);

                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 80, baos);
                pictureData = baos.toByteArray();

                float aspectRatio = bitmap.getWidth() / (float) bitmap.getHeight();

                int height = 500;
                int width = Math.round(height * aspectRatio);

                bitmap = Bitmap.createScaledBitmap(
                        bitmap, width, height, false);

                final Handler handler = new Handler();
                final Bitmap finalBitmap = bitmap;
                Runnable runnable = new Runnable() {
                    public void run() {

                        URL img_value = null;
                        Bitmap mIcon1 = null;

                        try {
                            handler.post(new Runnable() {
                                public void run() {
                                    civProfilePicture.setImageBitmap(finalBitmap);
                                }
                            });

                        } catch (Exception e) {
                            Log.e("IOException", "onActivityResult");
                            e.printStackTrace();
                        }


                    }
                };
                new Thread(runnable).start();

                FirebaseStorage storage = FirebaseStorage.getInstance();
                // Create a storage reference from our app
                StorageReference storageRef = storage.getReference();
                // Create a reference to profile picture
                StorageReference photoRef = storageRef.child("images/client_profile_image/" + currentUser.uid + "/profile_image.jpeg");

                // Upload photo to firebase
                UploadTask uploadTask = photoRef.putBytes(pictureData);
                uploadTask.addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception exception) {
                        // Handle unsuccessful uploads
                        Log.e("onActivityResult", "Upload Faliure: " + exception.toString());
                    }
                }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                        Log.d("onActivityResult", "Upload SUCCESS: ");
                        // taskSnapshot.getMetadata() contains file metadata such as size, content-type, and download URL.
                        Uri downloadUrl = taskSnapshot.getTask().getResult().getUploadSessionUri();

                        currentUser.profilePictureURL = downloadUrl.toString();

                        CurrentUser.getInstance(getApplicationContext()).persist(currentUser, new UpdateUserToDatabaseInterface() {
                            @Override
                            public void onUserUpdated() {
                            }

                            @Override
                            public void onError() {
                                Log.e(TAG, "Error saving user to database");
                            }
                        });
                        CurrentUser.getInstance(getApplicationContext()).cacheUser(currentUser);

                    }
                });

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }


    private void takePhotoOrChoose() {


        new AlertDialog.Builder(ProfileActivity.this)
                .setMessage("Select Destination")
                .setPositiveButton("Camera", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        // Request to take a photo
                        Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                        startActivityForResult(cameraIntent, 1888);
                    }
                })
                .setNegativeButton("Gallery", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        Intent pickPhoto = new Intent(Intent.ACTION_PICK,
                                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                        startActivityForResult(pickPhoto, 1889);
                    }
                })
                .create()
                .show();


    }


}


