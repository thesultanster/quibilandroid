package com.food.quibil.data_layer;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.food.quibil.interfaces.UpdateUserToDatabaseInterface;
import com.food.quibil.interfaces.UserServiceInterface;
import com.food.quibil.models.User;
import com.food.quibil.network_layer.UserService;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.gson.Gson;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by sultankhan on 7/22/17.
 */

public class CurrentUser {
    private String TAG = "CurrentUser";

    public User data;
    private Context context;
    private FirebaseAuth mAuth;
    FirebaseDatabase database;
    SharedPreferences prefs;
    private static CurrentUser instance = null;

    // Normal Initialization gets user from cache
    protected CurrentUser(Context context) {
        initializeUser(context);
    }

    public static CurrentUser getInstance(Context context) {
        if(instance == null) {
            instance = new CurrentUser(context);
        }
        return instance;
    }



    private void initializeUser(Context context){
        this.context = context;
        this.database = FirebaseDatabase.getInstance();
        mAuth = FirebaseAuth.getInstance();
        data = new User();
        prefs = context.getSharedPreferences("current_user", MODE_PRIVATE);
    }

    public void fetch(final UserServiceInterface userServiceInterface){
        UserService.getInstance(context).getUserFromDatabase(getUid(), new UserServiceInterface() {
            @Override
            public void onDataFetched(User user) {
                data = user;
                cacheUser(user);
                userServiceInterface.onDataFetched(user);
            }

            @Override
            public void onUserDataNotFound() {
                data = new User();
                userServiceInterface.onUserDataNotFound();
            }
        });
    }

    public void persist(final User currentUser, UpdateUserToDatabaseInterface updateUserToDatabaseInterface){
        UserService.getInstance(context).updateUserToDatabase(currentUser, updateUserToDatabaseInterface);
        cacheUser(currentUser);
    }

    public String getUid(){
        return FirebaseAuth.getInstance().getCurrentUser().getUid();
    }

    public void setProfileBitmap(Bitmap profileBitmap){
        data.profilePictureBitmap = profileBitmap;
    }


    public boolean isLoggedIn(){
        return mAuth.getCurrentUser() != null;
    }

    public void cacheUser(User user) {
        SharedPreferences.Editor prefsEditor = prefs.edit();
        Gson gson = new Gson();
        String json = gson.toJson(user);
        prefsEditor.putString("user", json);
        prefsEditor.commit();
    }


    public User getCache() {
        Gson gson = new Gson();
        String json = prefs.getString("user", "");
        return gson.fromJson(json, User.class);
    }






}
