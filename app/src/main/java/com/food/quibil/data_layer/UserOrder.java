package com.food.quibil.data_layer;

import android.content.Context;
import android.content.SharedPreferences;

import com.food.quibil.interfaces.AddItemToOrderInterface;
import com.food.quibil.interfaces.UpdateUserToDatabaseInterface;
import com.food.quibil.models.AddOnItem;
import com.food.quibil.models.MenuItem;
import com.food.quibil.models.Order;
import com.food.quibil.models.OrderedItem;
import com.food.quibil.models.User;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by sultankhan on 7/22/17.
 */

public class UserOrder {
    private String TAG = "UserOrder";

    private Context context;
    private String sessionId;
    private Order order;
    SharedPreferences prefs;
    private static UserOrder instance = null;


    // Normal Initialization gets user from cache
    private UserOrder() {
    }


    public static UserOrder getInstance(Context context){
        if(instance == null) {
            return instance = new UserOrder(context);
        }

        return instance;
    }

    private UserOrder(Context context) {
        this.context = context;
        this.order = new Order();
        this.prefs = context.getSharedPreferences("current_user", MODE_PRIVATE);
    }

    public Order getOrder() {
        return order;
    }

    public void persistSessionId(String sessionId){

        // Cache user
        User currentUser = CurrentUser.getInstance(context).getCache();
        currentUser.currentSession = sessionId;
        CurrentUser.getInstance(context).persist(currentUser, new UpdateUserToDatabaseInterface() {
            @Override
            public void onUserUpdated() {

            }

            @Override
            public void onError() {

            }
        });

        SharedPreferences.Editor prefsEditor = prefs.edit();
        prefsEditor.putString("sessionId", sessionId);
        prefsEditor.commit();
    }

    public void persistSessionState(String sessionState){

        // Cache user
        User currentUser = CurrentUser.getInstance(context).getCache();
        currentUser.currentSessionState = sessionState;
        CurrentUser.getInstance(context).persist(currentUser, new UpdateUserToDatabaseInterface() {
            @Override
            public void onUserUpdated() {

            }

            @Override
            public void onError() {

            }
        });

        SharedPreferences.Editor prefsEditor = prefs.edit();
        prefsEditor.putString("sessionState", sessionState);
        prefsEditor.commit();
    }

    public String getSessionState(){
        return prefs.getString("sessionState","CHECKED_OUT");
    }

    public String getSessionId(){
        return prefs.getString("sessionId","");
    }

    public void cacheRestaurantId(String restaurantId){
        SharedPreferences.Editor prefsEditor = prefs.edit();
        prefsEditor.putString("restaurantId", restaurantId);
        prefsEditor.commit();
    }

    public String getRestaurantId(){
        return prefs.getString("restaurantId","");
    }



    public void addItemToOrder(MenuItem menuItem, AddItemToOrderInterface addItemToOrderInterface){

        // Map to Ordered Items
        OrderedItem orderedItem = new OrderedItem();
        orderedItem.addOnItems = menuItem.addOnItems;
        orderedItem.categoryId = menuItem.categoryId;
        orderedItem.description = menuItem.description;
        orderedItem.id = menuItem.id;
        orderedItem.itemPhotoUrl = menuItem.itemPhotoUrl;
        orderedItem.name = menuItem.name;
        orderedItem.orderedAt = System.currentTimeMillis();
        orderedItem.price = menuItem.price;
        orderedItem.restaurantId = menuItem.restaurantId;
        orderedItem.state = menuItem.state;
        orderedItem.taxRate = menuItem.taxRate;

        order.orderedItems.add(orderedItem);
        addItemToOrderInterface.onItemAdded(orderedItem, getTotalCartPrice());
    }

    public void removeItemFromOrder(OrderedItem orderedItem){
        order.orderedItems.remove(orderedItem);
    }

    public void removeAddOnItemFromMenuItem(AddOnItem addOnItem){
        for(OrderedItem orderedItem : order.orderedItems){
            // Find out which menu item this add on item belongs to
            if(orderedItem.id.equals(addOnItem.menuItemId)){
                // Remove add on item from that menuItem;
                orderedItem.addOnItems.remove(addOnItem);
            }
        }
    }



    public int getTotalCartPrice(){
        int totalPrice = 0;

        // Sum up each item
        for(OrderedItem orderedItem :  order.orderedItems){

            // Sum up add on items
            for(AddOnItem item : orderedItem.addOnItems){
                totalPrice += item.price;
            }

            totalPrice += orderedItem.price;
        }

        return totalPrice;
    }

    public int getTotalNumberOfItemsInCard(){
        int totalNumberOfItems = 0;

        for(OrderedItem orderedItem :  order.orderedItems){
            totalNumberOfItems++;
        }

        return totalNumberOfItems;
    }


    public void setOrder(Order order) {
        this.order = order;
    }
}
