package com.food.quibil.network_layer;

import android.content.Context;
import android.content.SharedPreferences;

import com.food.quibil.interfaces.FetchRestaurantItemAddOnsInterface;
import com.food.quibil.interfaces.FetchRestaurantMenuCategoriesInterface;
import com.food.quibil.interfaces.FetchRestaurantMenuItemsInterface;
import com.food.quibil.models.AddOnItem;
import com.food.quibil.models.MenuCategory;
import com.food.quibil.models.MenuItem;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by sultankhan on 8/1/17.
 */

public class MenuService {

    String TAG = "Network Layer: MenuService";

    private DatabaseReference mDatabase;
    private Context context;
    private SharedPreferences prefs;
    private static MenuService instance;

    public static MenuService getInstance(Context context) {

        if (instance == null) {
            instance = new MenuService(context);
        }
        return instance;
    }

    private MenuService(Context context) {
        this.mDatabase = FirebaseDatabase.getInstance().getReference();
        this.context = context;
        this.prefs = context.getSharedPreferences("current_user", MODE_PRIVATE);
    }


    public void getMenuCategories(final String restaurantId, final FetchRestaurantMenuCategoriesInterface fetchRestaurantMenuInterface) {
        FirebaseFirestore.getInstance()
                .collection("restaurants").document(restaurantId)
                .collection("menu").get().addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
            @Override
            public void onSuccess(QuerySnapshot queryDocumentSnapshots) {
                for (DocumentSnapshot documentSnapshot : queryDocumentSnapshots.getDocuments()) {
                    MenuCategory menuCategory = documentSnapshot.toObject(MenuCategory.class);
                    fetchRestaurantMenuInterface.onDataFetched(menuCategory);
                }
            }
        });
    }

    public void getMenuItems(final String restaurantId, final String categoryId, final FetchRestaurantMenuItemsInterface fetchRestaurantMenuItemsInterface) {
        FirebaseFirestore.getInstance()
                .collection("restaurants").document(restaurantId)
                .collection("menu").document(categoryId)
                .collection("items").get().addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
            @Override
            public void onSuccess(QuerySnapshot queryDocumentSnapshots) {
                for (DocumentSnapshot documentSnapshot : queryDocumentSnapshots.getDocuments()) {
                    MenuItem menuItem = documentSnapshot.toObject(MenuItem.class);
                    fetchRestaurantMenuItemsInterface.onDataFetched(menuItem);
                }
            }
        });
    }

    public void getItemAddOns(final String restaurantId, final String categoryId, final String itemId, final FetchRestaurantItemAddOnsInterface fetchRestaurantItemAddOnsInterface) {
        FirebaseFirestore.getInstance()
                .collection("restaurants").document(restaurantId)
                .collection("menu").document(categoryId)
                .collection("items").document(itemId)
                .collection("addOns").get().addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
            @Override
            public void onSuccess(QuerySnapshot queryDocumentSnapshots) {
                for (DocumentSnapshot documentSnapshot : queryDocumentSnapshots.getDocuments()) {
                    AddOnItem addOnItem = documentSnapshot.toObject(AddOnItem.class);
                    fetchRestaurantItemAddOnsInterface.onDataFetched(addOnItem);
                }
            }
        });
    }

}
