package com.food.quibil.network_layer

import android.content.Context
import android.util.Log
import com.food.quibil.data_layer.UserOrder
import com.food.quibil.models.Session
import com.food.quibil.ui.features.restaurantmanager.SessionStateInterface
import com.google.firebase.database.*


class SessionManager {

    private val TAG = "SessionManager"
    private lateinit var mDatabase: DatabaseReference
    private lateinit var sessionListener: ValueEventListener

    fun init(sessionStateInterface: SessionStateInterface, sessionId: String) {

        mDatabase = FirebaseDatabase.getInstance().reference
        sessionListener = object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                val session = dataSnapshot.getValue<Session>(Session::class.java)

                when(session!!.state){
                    "CHECKED_OUT" -> sessionStateInterface.checkedOut(session)
                }
            }

            override fun onCancelled(databaseError: DatabaseError) {
                // Getting data failed, log a message
                Log.e(TAG, "fetch session:onCancelled", databaseError.toException())
            }
        }
        mDatabase.child("order_sessions")
                .child(sessionId)
                .addValueEventListener(sessionListener)
    }

    fun release() {
        mDatabase.removeEventListener(sessionListener)
    }
}


