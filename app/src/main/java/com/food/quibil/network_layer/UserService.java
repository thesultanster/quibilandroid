package com.food.quibil.network_layer;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.util.Log;

import com.food.quibil.data_layer.CurrentUser;
import com.food.quibil.interfaces.FetchPastSessionsInterface;
import com.food.quibil.interfaces.FetchRestaurantMenuCategoriesInterface;
import com.food.quibil.interfaces.UpdateUserToDatabaseInterface;
import com.food.quibil.interfaces.UserServiceInterface;
import com.food.quibil.models.MenuCategory;
import com.food.quibil.models.Session;
import com.food.quibil.models.User;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by sultankhan on 8/1/17.
 */

public class UserService {
    private String TAG = "UserService";

    private DatabaseReference mDatabase;
    private Context context;
    private SharedPreferences prefs;
    private static UserService instance;

    public static UserService getInstance(Context context) {

        if (instance == null) {
            instance = new UserService(context);
        }
        return instance;
    }

    private UserService(Context context) {
        this.mDatabase = FirebaseDatabase.getInstance().getReference();
        this.context = context;
        this.prefs = context.getSharedPreferences("current_user", MODE_PRIVATE);
    }

    public void getUserFromDatabase(final String uid, final UserServiceInterface userServiceInterface) {
        DocumentReference docRef = FirebaseFirestore.getInstance().collection("users").document(uid);
        docRef.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if (task.isSuccessful()) {
                    DocumentSnapshot document = task.getResult();
                    if (document.exists()) {
                        Log.d(TAG, "DocumentSnapshot data: " + document.getData());

                        User currentUser = document.toObject(User.class);

                        userServiceInterface.onDataFetched(currentUser);
                    } else {
                        Log.d(TAG, "No such document");
                        userServiceInterface.onUserDataNotFound();
                    }
                } else {
                    Log.d(TAG, "get failed with ", task.getException());
                    userServiceInterface.onUserDataNotFound();
                }
            }
        });

    }


    public void getOlderSessions(final FetchPastSessionsInterface fetchPastSessionsInterface) {
        FirebaseFirestore.getInstance()
                .collection("clientSessionHistory").document(CurrentUser.getInstance(context).getUid())
                .collection("closedOutSessions").get().addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
            @Override
            public void onSuccess(QuerySnapshot queryDocumentSnapshots) {

                if(queryDocumentSnapshots.getDocuments().size() == 0){
                    fetchPastSessionsInterface.onClearList();
                } else {
                    for (DocumentSnapshot documentSnapshot : queryDocumentSnapshots.getDocuments()) {
                        Session session = documentSnapshot.toObject(Session.class);
                        fetchPastSessionsInterface.onDataFetched(session);
                    }
                }
            }
        });
    }

    public void updateUserToDatabase(final User user, final UpdateUserToDatabaseInterface updateUserToDatabaseInterface) {
        DocumentReference docRef = FirebaseFirestore.getInstance().collection("users").document(user.uid);
        docRef.set(user).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                updateUserToDatabaseInterface.onUserUpdated();
            }
        });

    }


}
