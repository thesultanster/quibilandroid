package com.food.quibil.network_layer;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.food.quibil.data_layer.CurrentUser;
import com.food.quibil.data_layer.UserOrder;
import com.food.quibil.interfaces.CloseOutClientInterface;
import com.food.quibil.interfaces.FetchQRCode;
import com.food.quibil.interfaces.OnCheckOutUserFromRestaurantInterface;
import com.food.quibil.interfaces.OnCheckinUserToRestaurantInterface;
import com.food.quibil.interfaces.OnRequestFoodForSessionInterface;
import com.food.quibil.models.MenuItem;
import com.food.quibil.models.Order;
import com.food.quibil.models.OrderedItem;
import com.food.quibil.models.QRCode;
import com.food.quibil.models.Session;
import com.food.quibil.models.User;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONArray;
import org.json.JSONObject;

import static android.content.Context.MODE_PRIVATE;

public class OrderService {
    private String TAG = "OrderService";

    private Context context;
    private SharedPreferences prefs;
    private static OrderService instance;

    public static OrderService getInstance(Context context) {

        if (instance == null) {
            instance = new OrderService(context);
        }
        return instance;
    }

    private OrderService(Context context) {
        this.context = context;
        this.prefs = context.getSharedPreferences("current_user", MODE_PRIVATE);
    }


    public void requestFoodForSession(final OnRequestFoodForSessionInterface onRequestFoodForSessionInterface) {

        JSONObject postBody = null;
        JSONArray orders = new JSONArray();


        for (OrderedItem orderedItem : UserOrder.getInstance(context).getOrder().orderedItems) {
            Gson gson = new GsonBuilder().create();
            String stringItem = gson.toJson(orderedItem);

            try {
                JSONObject jsonItem = new JSONObject(stringItem);
                jsonItem.put("createdAt",System.currentTimeMillis());
                orders.put(jsonItem);
            } catch (Exception e) {
                Log.e(TAG, e.getMessage());
            }
        }

        try {
            postBody = new JSONObject();
            postBody.put("sessionId", UserOrder.getInstance(context).getSessionId());
            postBody.put("orders", orders);
            postBody.put("totalPaymentAmount", UserOrder.getInstance(context).getTotalCartPrice());
        } catch (Exception e) {

        }

        int socketTimeout = 30000; // 30 seconds. You can change it
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);

        Log.i(TAG, "API Call: requestFoodForSession");

        String postURL = "https://us-central1-quibil-b5335.cloudfunctions.net/requestFoodForSession";
        JsonObjectRequest postRequest = new JsonObjectRequest(Request.Method.POST, postURL, postBody,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            Log.i(TAG, "JSON response: " + response);
                            JSONObject jsonResponse = response;

                            // If SUCCESS
                            if (jsonResponse.getInt("status") == 200) {
                                onRequestFoodForSessionInterface.onRequestSent();
                            } else {
                                Log.e("Error JSON", jsonResponse.getString("message"));
                                onRequestFoodForSessionInterface.onError();
                                return;
                            }
                        } catch (Exception e) {
                            Log.e(TAG, "onResponse: exception");
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e(TAG, "VolleyError:onErrorResponse");
                        error.printStackTrace();
                    }
                }
        ) {

        };
        postRequest.setRetryPolicy(policy);
        Volley.newRequestQueue(context).add(postRequest);

    }

    public void checkinUserToRestaurant(String restaurantId, String tableId, final OnCheckinUserToRestaurantInterface onCheckinUserToRestaurantInterface) {

        JSONObject postBody = null;
        try {
            postBody = new JSONObject();
            postBody.put("clientId", CurrentUser.getInstance(context).getUid());
            postBody.put("restaurantId", restaurantId);
            postBody.put("tableId", tableId);
        } catch (Exception e) {

        }

        int socketTimeout = 30000; // 30 seconds. You can change it
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);

        Log.i(TAG, "API Call: checkinUserToRestaurant");

        String postURL = "https://us-central1-quibil-b5335.cloudfunctions.net/checkinUserToRestaurant";
        JsonObjectRequest postRequest = new JsonObjectRequest(Request.Method.POST, postURL, postBody,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            Log.i(TAG, "JSON response: " + response);

                            // If SUCCESS
                            if (response.getInt("status") == 200) {
                                onCheckinUserToRestaurantInterface.onUserCheckedIn(response.getString("sessionId"));
                            } else {
                                Log.e("Error JSON", response.getString("message"));
                                onCheckinUserToRestaurantInterface.onError();
                                return;
                            }

                        } catch (Exception e) {
                            Log.e(TAG, "onResponse: exception");
                            onCheckinUserToRestaurantInterface.onError();
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e(TAG, "VolleyError:onErrorResponse");
                        error.printStackTrace();
                    }
                }
        ) {

        };


        postRequest.setRetryPolicy(policy);
        Volley.newRequestQueue(context).add(postRequest);

    }


    public void checkoutUserFromRestaurant(String restaurantId, String sessionId, final OnCheckOutUserFromRestaurantInterface checkOutUserFromRestaurantInterface) {

        if (restaurantId == null || restaurantId.equals("")) {
            Log.e(TAG, "Restaurant Id is empty");
            checkOutUserFromRestaurantInterface.onError();
            return;
        }
        if (sessionId == null || sessionId.equals("")) {
            Log.e(TAG, "Session Id is empty");
            checkOutUserFromRestaurantInterface.onError();
            return;
        }

        UserOrder.getInstance(context).persistSessionId("");
        UserOrder.getInstance(context).persistSessionState("CHECKED_OUT");
        UserOrder.getInstance(context).cacheRestaurantId("");
        UserOrder.getInstance(context).setOrder(new Order());

        FirebaseDatabase.getInstance().getReference()
                .child("restaurant_sessions")
                .child(restaurantId)
                .child("checkedIn")
                .child(sessionId).removeValue(new DatabaseReference.CompletionListener() {
            @Override
            public void onComplete(@Nullable DatabaseError databaseError, @NonNull DatabaseReference databaseReference) {

                checkOutUserFromRestaurantInterface.onUserCheckedOut();
            }
        });

    }

    public void closeOutClient(String nonce, String sessionId, long totalAmount, final CloseOutClientInterface closeOutClientInterface) {
        JSONObject postBody = null;
        try {
            postBody = new JSONObject();
            postBody.put("paymentMethodNonce", nonce);
            postBody.put("sessionId", sessionId);
            postBody.put("amount", totalAmount);
            Log.d(TAG, CurrentUser.getInstance(context).getUid());
        } catch (Exception e) {

        }

        int socketTimeout = 30000; // 30 seconds. You can change it
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);

        Log.i(TAG, "API Call: clientCloseOut");

        String postURL = "https://us-central1-quibil-b5335.cloudfunctions.net/clientCloseOut";
        JsonObjectRequest postRequest = new JsonObjectRequest(Request.Method.POST, postURL, postBody,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            Log.i(TAG, "JSON response: " + response);

                            // If SUCCESS
                            if (response.getInt("status") == 200) {
                                closeOutClientInterface.onClientClosedOut();
                            } else {
                                Log.e("Error JSON", response.getString("message"));
                                closeOutClientInterface.onError();
                                return;
                            }

                        } catch (Exception e) {
                            Log.e(TAG, "onResponse: exception");
                            closeOutClientInterface.onError();
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e(TAG, "VolleyError:onErrorResponse");
                        error.printStackTrace();
                        closeOutClientInterface.onError();
                    }
                }
        ) {

        };


        postRequest.setRetryPolicy(policy);
        Volley.newRequestQueue(context).add(postRequest);
    }


    //TODO: Create a QR Code repository
    public void getRestaurantdIdFromQRCode(String qrCodeId, final FetchQRCode fetchQRCode) {
        FirebaseFirestore.getInstance()
                .collection("qrcodes").document(qrCodeId)
                .get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                    @Override
                    public void onSuccess(DocumentSnapshot queryDocumentSnapshot) {
                        QRCode qrCode = queryDocumentSnapshot.toObject(QRCode.class);
                        fetchQRCode.onDataFetched(qrCode);
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        fetchQRCode.onError();
                    }
                });
    }


}
