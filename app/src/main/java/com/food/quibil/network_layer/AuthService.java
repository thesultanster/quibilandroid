package com.food.quibil.network_layer;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.util.Log;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.food.quibil.data_layer.CurrentUser;
import com.food.quibil.interfaces.GenerateBraintreeClientTokenInterface;
import com.food.quibil.interfaces.UpdateUserToDatabaseInterface;
import com.food.quibil.interfaces.UserServiceInterface;
import com.food.quibil.models.User;
import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.functions.FirebaseFunctions;
import com.google.firebase.functions.HttpsCallableResult;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import static android.content.Context.MODE_PRIVATE;

public class AuthService {
    private String TAG = "AuthService";

    private DatabaseReference mDatabase;
    private Context context;
    private SharedPreferences prefs;
    private static AuthService instance;

    public static AuthService getInstance(Context context) {

        if (instance == null) {
            instance = new AuthService(context);
        }
        return instance;
    }

    private AuthService(Context context) {
        this.mDatabase = FirebaseDatabase.getInstance().getReference();
        this.context = context;
        this.prefs = context.getSharedPreferences("current_user", MODE_PRIVATE);
    }

    public void generateBraintreeClientToken(final GenerateBraintreeClientTokenInterface generateBraintreeClientTokenInterface) {
        JSONObject postBody = null;
        try {
            postBody = new JSONObject();
            postBody.put("clientId", CurrentUser.getInstance(context).getUid());
            Log.d(TAG, CurrentUser.getInstance(context).getUid());
        } catch (Exception e) {

        }

        int socketTimeout = 30000; // 30 seconds. You can change it
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);

        Log.i(TAG, "API Call: generateBraintreeClientToken");

        String postURL = "https://us-central1-quibil-b5335.cloudfunctions.net/generateBTClientToken";
        JsonObjectRequest postRequest = new JsonObjectRequest(Request.Method.POST, postURL, postBody,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            Log.i(TAG, "JSON response: " + response);

                            // If SUCCESS
                            if (response.getInt("status") == 200) {
                                generateBraintreeClientTokenInterface.onClientTokenCreated(response.getString("clientToken"));
                            } else {
                                Log.e("Error JSON", response.getString("message"));
                                generateBraintreeClientTokenInterface.onError();
                                return;
                            }

                        } catch (Exception e) {
                            Log.e(TAG, "onResponse: exception");
                            generateBraintreeClientTokenInterface.onError();
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e(TAG, "VolleyError:onErrorResponse");
                        error.printStackTrace();
                    }
                }
        ) {

        };


        postRequest.setRetryPolicy(policy);
        Volley.newRequestQueue(context).add(postRequest);
    }

    public void testFindClient(final GenerateBraintreeClientTokenInterface generateBraintreeClientTokenInterface) {
        JSONObject postBody = null;
        try {
            postBody = new JSONObject();
            postBody.put("customerId", CurrentUser.getInstance(context).getUid());
            Log.d(TAG, CurrentUser.getInstance(context).getUid());
        } catch (Exception e) {

        }

        int socketTimeout = 30000; // 30 seconds. You can change it
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);

        Log.i(TAG, "API Call: testBTFindCustomer");

        String postURL = "https://us-central1-quibil-b5335.cloudfunctions.net/testBTFindCustomer";
        JsonObjectRequest postRequest = new JsonObjectRequest(Request.Method.POST, postURL, postBody,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            Log.i(TAG, "JSON response: " + response);

                            // If SUCCESS
                            if (response.getInt("status") == 200) {
                                //generateBraintreeClientTokenInterface.onClientTokenCreated(response.getString("clientToken"));
                            } else {
                                Log.e("Error JSON", response.getString("message"));
                                generateBraintreeClientTokenInterface.onError();
                                return;
                            }

                        } catch (Exception e) {
                            Log.e(TAG, "onResponse: exception");
                            generateBraintreeClientTokenInterface.onError();
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e(TAG, "VolleyError:onErrorResponse");
                        error.printStackTrace();
                    }
                }
        ) {

        };


        postRequest.setRetryPolicy(policy);
        Volley.newRequestQueue(context).add(postRequest);
    }


}
