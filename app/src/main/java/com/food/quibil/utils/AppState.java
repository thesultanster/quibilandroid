package com.food.quibil.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.util.Log;

import com.food.quibil.interfaces.UpdateUserToDatabaseInterface;
import com.food.quibil.interfaces.UserServiceInterface;
import com.food.quibil.models.User;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by sultankhan on 8/1/17.
 */

public class AppState {
    private String TAG = "AppState";

    private DatabaseReference mDatabase;
    private Context context;
    private SharedPreferences prefs;
    private static AppState instance;

    public static AppState getInstance(Context context) {

        if (instance == null) {
            instance = new AppState(context);
        }
        return instance;
    }

    private AppState(Context context) {
        this.mDatabase = FirebaseDatabase.getInstance().getReference();
        this.context = context;
        this.prefs = context.getSharedPreferences("current_user", MODE_PRIVATE);
    }

    public void cacheBraintreeClientToken(String clientToken){
        prefs.edit().putString("clientToken", clientToken).commit();
    }

    public String getBraintreeClientToken(){
        return prefs.getString("clientToken", "");
    }

}
