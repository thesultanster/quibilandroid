package com.food.quibil.utils;

import android.app.Application;
import android.content.Context;
import android.util.Log;

import com.google.firebase.analytics.FirebaseAnalytics;

/**
 * Created by sultankhan on 1/22/18.
 */

public class App extends Application {

    private static FirebaseAnalytics firebaseAnalytics;

    @Override
    public void onCreate() {
        super.onCreate();

        firebaseAnalytics = FirebaseAnalytics.getInstance(this);
    }



    public static FirebaseAnalytics getmFirebaseAnalytics() {
        return firebaseAnalytics;
    }

}
