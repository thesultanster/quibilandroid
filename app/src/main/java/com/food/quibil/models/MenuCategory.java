package com.food.quibil.models;

import java.io.Serializable;

/**
 * Created by sultankhan on 7/24/18.
 */

public class MenuCategory implements Serializable{
    public String id;
    public String name;
    public String restaurantId;
}
