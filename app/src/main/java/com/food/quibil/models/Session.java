package com.food.quibil.models;

import java.util.ArrayList;

/**
 * Created by sultankhan on 8/28/18.
 */

public class Session {

    public String id;
    public String clientId;
    public String restaurantId;
    public String restaurantName;
    public String restaurantAddress;
    public String restaurantProfilePicURL;
    public String tableId;
    public String userFullName;
    public String userProfilePicURL;
    public String state;
    public ArrayList<OrderedItem> orders;
    public long updatedAt;
    public long createdAt;
    public long totalPaymentAmount;
}
