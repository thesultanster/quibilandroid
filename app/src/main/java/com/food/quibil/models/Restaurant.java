package com.food.quibil.models;

/**
 * Created by sultankhan on 7/24/18.
 */

public class Restaurant {

    public String id="";
    public String name="";
    public String address="";
    public String phoneNumber="";
    public String profilePictureUrl="";
}
