package com.food.quibil.models;

import com.food.quibil.interfaces.RoundItemRowType;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by sultankhan on 7/24/18.
 */

public class MenuItem implements Serializable, RoundItemRowType {

    public String id;
    public String name;
    public String description;
    public String restaurantId;
    public String categoryId;
    public String itemPhotoUrl;
    public String state;
    public int index;
    public int price;
    public double taxRate;
    public ArrayList<AddOnItem> addOnItems = new ArrayList<>();
}
