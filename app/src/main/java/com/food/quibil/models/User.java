package com.food.quibil.models;

import android.graphics.Bitmap;

import java.io.Serializable;

/**
 * Created by sultankhan on 7/22/17.
 */

public class User implements Serializable{

    public String uid = "";
    public String firstName = "";
    public String lastName = "";
    public String email = "";
    public String profilePictureURL = "";
    public String profileType = "CLIENT";
    public String phoneNumber;
    public Bitmap profilePictureBitmap = null;
    public String currentSession = "";
    public String currentSessionState = "CHECKED_OUT";

    public User(){

    }

}
