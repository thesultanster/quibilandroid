package com.food.quibil.models;

import com.food.quibil.interfaces.RoundItemRowType;

import java.io.Serializable;
import java.util.ArrayList;

public class OrderedItem implements Serializable, RoundItemRowType {

    public String id;
    public String name;
    public String description;
    public String restaurantId;
    public String categoryId;
    public String itemPhotoUrl;
    public String state;
    public int price;
    public double taxRate;
    public long orderedAt;
    public ArrayList<AddOnItem> addOnItems = new ArrayList<>();

    public OrderedItem(){

    }

}
