package com.food.quibil.interfaces;


import com.food.quibil.models.QRCode;
import com.food.quibil.models.Session;

public interface FetchQRCode {
    void onDataFetched(QRCode qrCode);
    void onError();

}
