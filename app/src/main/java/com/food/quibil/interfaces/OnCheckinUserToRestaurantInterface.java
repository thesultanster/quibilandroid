package com.food.quibil.interfaces;


/**
 * Created by sultankhan on 8/1/17.
 */

public interface OnCheckinUserToRestaurantInterface {
    void onUserCheckedIn(String sessionId);
    void onError();
}
