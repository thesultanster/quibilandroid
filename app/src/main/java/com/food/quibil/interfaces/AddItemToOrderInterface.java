package com.food.quibil.interfaces;


import com.food.quibil.models.OrderedItem;

/**
 * Created by sultankhan on 8/1/17.
 */

public interface AddItemToOrderInterface {
    void onItemAdded(OrderedItem orderedItem, int newTotalCartPrice);

}
