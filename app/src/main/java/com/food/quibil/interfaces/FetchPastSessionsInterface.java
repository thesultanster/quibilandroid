package com.food.quibil.interfaces;


import com.food.quibil.models.Restaurant;
import com.food.quibil.models.Session;

public interface FetchPastSessionsInterface {
    void onDataFetched(Session session);
    void onClearList();
    void onError();

}
