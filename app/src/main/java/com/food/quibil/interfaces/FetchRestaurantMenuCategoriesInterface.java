package com.food.quibil.interfaces;


import com.food.quibil.models.MenuCategory;

/**
 * Created by sultankhan on 8/1/17.
 */

public interface FetchRestaurantMenuCategoriesInterface {
    void onDataFetched(MenuCategory menuCategory);
    void onError();

}
