package com.food.quibil.interfaces;


import com.food.quibil.models.AddOnItem;
import com.food.quibil.models.MenuItem;

/**
 * Created by sultankhan on 8/1/17.
 */

public interface OnAddOnItemSelectedInterface {
    void onAddOnItemIncremented(int position, AddOnItem addOnItem);
    void onAddOnItemDecremented(int position, AddOnItem addOnItem);
}
