package com.food.quibil.interfaces;

import com.food.quibil.models.User;

/**
 * Created by sultankhan on 7/22/17.
 */

public interface onUserDataChange {
    void onDataSaved(User user);
    void onError();
}
