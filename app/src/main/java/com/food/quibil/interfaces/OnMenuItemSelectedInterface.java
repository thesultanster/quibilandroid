package com.food.quibil.interfaces;


import com.food.quibil.models.MenuItem;

/**
 * Created by sultankhan on 8/1/17.
 */

public interface OnMenuItemSelectedInterface {
    void onItemSelected(int position, MenuItem menuItem);
}
