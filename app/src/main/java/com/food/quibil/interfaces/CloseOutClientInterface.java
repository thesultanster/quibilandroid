package com.food.quibil.interfaces;


public interface CloseOutClientInterface {
    void onClientClosedOut();
    void onError();

}
