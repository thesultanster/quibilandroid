package com.food.quibil.interfaces;


import com.food.quibil.models.Restaurant;

/**
 * Created by sultankhan on 8/1/17.
 */

public interface FetchRestaurantInterface {
    void onDataFetched(Restaurant restaurant);
    void onError();

}
