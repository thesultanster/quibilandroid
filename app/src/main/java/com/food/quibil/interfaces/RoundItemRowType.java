package com.food.quibil.interfaces;

/**
 * Created by sultankhan on 8/18/18.
 */

public interface RoundItemRowType {
    int ITEM = 0;
    int SUB_ITEM = 1;
}

