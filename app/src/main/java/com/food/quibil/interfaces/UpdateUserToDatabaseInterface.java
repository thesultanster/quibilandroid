package com.food.quibil.interfaces;


import com.food.quibil.models.User;

/**
 * Created by sultankhan on 8/1/17.
 */

public interface UpdateUserToDatabaseInterface {
    void onUserUpdated();
    void onError();

}
