package com.food.quibil.interfaces;


import com.food.quibil.models.MenuItem;

/**
 * Created by sultankhan on 8/1/17.
 */

public interface RoundItemInterface {
    void onUserWantsToRemoveItem(int position, RoundItemRowType type);
}
